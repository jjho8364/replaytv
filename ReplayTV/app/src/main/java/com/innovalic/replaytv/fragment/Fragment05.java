package com.innovalic.replaytv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovalic.replaytv.R;
import com.innovalic.replaytv.activity.MidListActivity;
import com.innovalic.replaytv.activity.VideoListActivity;
import com.innovalic.replaytv.adapter.GridViewAdapter;
import com.innovalic.replaytv.adapter.MidListViewAdapter;
import com.innovalic.replaytv.model.GridViewItem;
import com.innovalic.replaytv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-02.
 */
public class Fragment05 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment05 - ";
    private ListView listView;
    private ArrayList<String> listArr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        String model = Build.MODEL.toLowerCase();
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel=Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        //Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //int contactsCnt = c.getCount();
        //if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "operatorFlag : " + operatorFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ( (model.equals("sph-d720") || model.contains("nexus") || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment05, container, false);
            listArr = new ArrayList<String>();
            listArr.add("슈퍼걸 시즌1");
            listArr.add("왕좌의게임 시즌1");
            listArr.add("왕좌의게임 시즌2");
            listArr.add("왕좌의게임 시즌3");
            listArr.add("왕좌의게임 시즌4");
            listArr.add("왕좌의게임 시즌5");
            listArr.add("왕좌의게임 시즌6");
            listArr.add("워킹데드 시즌1");
            listArr.add("워킹데드 시즌2");
            listArr.add("워킹데드 시즌3");
            listArr.add("워킹데드 시즌4");
            listArr.add("워킹데드 시즌5");
            listArr.add("워킹데드 시즌6");
            listArr.add("굿 와이프 시즌1");
            listArr.add("굿 와이프 시즌2");
            listArr.add("굿 와이프 시즌3");
            //listArr.add("굿 와이프 시즌4");
            //listArr.add("굿 와이프 시즌5");
            //listArr.add("굿 와이프 시즌6");
            //listArr.add("굿 와이프 시즌7");
            //listArr.add("굿 와이프 시즌8");
            listArr.add("하우스 오브 카드 시즌1");
            listArr.add("하우스 오브 카드 시즌2");
            listArr.add("하우스 오브 카드 시즌3");
            listArr.add("히어로즈 시즌1");
            listArr.add("히어로즈 시즌2");
            listArr.add("히어로즈 시즌3");
            listArr.add("히어로즈 시즌4");
            listArr.add("고담 시즌1");
            listArr.add("고담 시즌2");
            listArr.add("데어데블 시즌1");
            listArr.add("데어데블 시즌2");
            listArr.add("바이닐 시즌1");
            listArr.add("샨나라 연대기 시즌1");
            listArr.add("빅뱅 이론 시즌1");
            listArr.add("빅뱅 이론 시즌2");
            listArr.add("리미트리스 시즌1");
            listArr.add("루시퍼 시즌1");
            listArr.add("주(Zoo) 시즌1");
            listArr.add("주(Zoo) 시즌2");
            listArr.add("플래시 시즌1");
            listArr.add("플래시 시즌2");
            listArr.add("마이너리티 리포트 시즌1");
            listArr.add("익스팬스");
            listArr.add("DC 레전드 오브 투모로우");
            listArr.add("닥터후 시즌1");
            listArr.add("닥터후 시즌2");
            listArr.add("닥터후 시즌3");
            listArr.add("닥터후 시즌4");
            listArr.add("닥터후 시즌5");
            listArr.add("닥터후 시즌6");
            listArr.add("닥터후 시즌7");
            listArr.add("닥터후 시즌8");
            listArr.add("닥터후 시즌9");

            listView = (ListView)view.findViewById(R.id.listview2);
            listView.setAdapter(new MidListViewAdapter(getActivity(), getActivity().getLayoutInflater(), listArr));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), MidListActivity.class);
                    intent.putExtra("title", listArr.get(position));
                    intent.putExtra("type", "mid");
                    startActivity(intent);
                }
            });
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
