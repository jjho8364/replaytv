package com.innovalic.replaytv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovalic.replaytv.R;
import com.innovalic.replaytv.adapter.ListViewAdapter2;
import com.innovalic.replaytv.model.GetEnglandListARr;
import com.innovalic.replaytv.model.GetJapanListArr;
import com.innovalic.replaytv.model.GetMidListArr;
import com.innovalic.replaytv.model.ListViewItem2;

import java.util.ArrayList;

public class MidListActivity extends Activity implements AdapterView.OnItemClickListener {
    private final String TAG = " MidListActivity -  ";
    private ProgressDialog mProgressDialog;
    String title = "";

    ListView listView2;
    ArrayList<ListViewItem2> listArr;

    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mid_list);

        AdView mAdViewMidUpper = (AdView) findViewById(R.id.adView_mid_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewMidUpper.loadAd(adRequest);

        Intent intent = getIntent();
        title = (String)intent.getSerializableExtra("title");
        type = (String)intent.getSerializableExtra("type");

        listArr = new ArrayList<ListViewItem2>();
        classifyTitle(type);

        listView2 = (ListView)findViewById(R.id.listview2);

        listView2.setAdapter(new ListViewAdapter2(MidListActivity.this, getLayoutInflater(), listArr));
        listView2.setOnItemClickListener(this);

    }

    public void classifyTitle(String type){
        Log.d(TAG, title);
        GetMidListArr getMidListArr = new GetMidListArr();
        GetJapanListArr getJapanListArr = new GetJapanListArr();
        GetEnglandListARr getEnglandListArr = new GetEnglandListARr();
        switch (type){
            case "mid" :
                listArr = getMidListArr.getListArr(title);
                break;
            case "ild" :
                listArr = getJapanListArr.getListArr(title);
                break;
            case "youngd" :
                listArr = getEnglandListArr.getListArr(title);
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
        intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
        intent.putExtra("tag", "1");
        startActivity(intent);
    }
}
