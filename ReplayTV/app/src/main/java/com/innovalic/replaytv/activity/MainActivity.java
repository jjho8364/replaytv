package com.innovalic.replaytv.activity;

import android.content.Intent;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.innovalic.replaytv.R;
import com.innovalic.replaytv.fragment.Fragment01;
import com.innovalic.replaytv.fragment.Fragment02;
import com.innovalic.replaytv.fragment.Fragment03;
import com.innovalic.replaytv.fragment.Fragment04;
import com.innovalic.replaytv.fragment.Fragment05;
import com.innovalic.replaytv.fragment.Fragment06;
import com.innovalic.replaytv.fragment.Fragment20;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private final String TAG = " MainActivity - ";

    private InterstitialAd interstitialAd;

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FORTH = 3;
    public final static int FRAGMENT_FIFTH = 4;
    //public final static int FRAGMENT_SIXTH = 5;
    public final static int FRAGMENT_TWENTYTH = 19;

    private TextView fragment01;
    private TextView fragment02;
    private TextView fragment03;
    private TextView fragment04;
    private TextView fragment05;
    //private TextView fragment06;
    private TextView fragment20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AdView mAdViewUpper = (AdView) findViewById(R.id.adView_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewUpper.loadAd(adRequest);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/8731631253");
        interstitialAd.loadAd(adRequest);

        fragment01 = (TextView)findViewById(R.id.tv_fragment01);
        fragment02 = (TextView)findViewById(R.id.tv_fragment02);
        fragment03 = (TextView)findViewById(R.id.tv_fragment03);
        fragment04 = (TextView)findViewById(R.id.tv_fragment04);
        fragment05 = (TextView)findViewById(R.id.tv_fragment05);
        /*fragment06 = (TextView)findViewById(R.id.tv_fragment06);*/
        fragment20 = (TextView)findViewById(R.id.tv_fragment20);

        fragment01.setOnClickListener(this);
        fragment02.setOnClickListener(this);
        fragment03.setOnClickListener(this);
        fragment04.setOnClickListener(this);
        fragment05.setOnClickListener(this);
        /* fragment06.setOnClickListener(this);*/
        fragment20.setOnClickListener(this);

        mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
        fragmentReplace(mCurrentFragmentIndex);


        Button btn = (Button)findViewById(R.id.stop_service);
        btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                offColorTv();
                fragment01.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                offColorTv();
                fragment02.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                fragment03.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                fragment04.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FORTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05:
                offColorTv();
                fragment05.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FIFTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            /*case R.id.tv_fragment06:
                offColorTv();
                fragment06.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_SIXTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;*/
            case R.id.tv_fragment20:
                offColorTv();
                fragment20.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.stop_service :
                Intent intent = new Intent(MainActivity.this, StopServiceActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment02();
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment03();
                break;
            case FRAGMENT_FORTH:
                newFragment = new Fragment04();
                break;
            case FRAGMENT_FIFTH:
                newFragment = new Fragment05();
                break;
            /*case FRAGMENT_SIXTH:
                newFragment = new Fragment06();
                break;*/
            case FRAGMENT_TWENTYTH:
                newFragment = new Fragment20();
                break;
            default:
                Log.d(TAG, "Unhandle case");
                break;
        }

        return newFragment;
    }

    public void offColorTv(){
        fragment01.setBackgroundResource(R.drawable.gridview_border);
        fragment02.setBackgroundResource(R.drawable.gridview_border);
        fragment03.setBackgroundResource(R.drawable.gridview_border);
        fragment04.setBackgroundResource(R.drawable.gridview_border);
        fragment05.setBackgroundResource(R.drawable.gridview_border);
        /*fragment06.setBackgroundResource(R.drawable.gridview_border);*/
        fragment20.setBackgroundResource(R.drawable.gridview_border);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        interstitialAd.show();
    }

}
