package com.innovalic.replaytv.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.innovalic.replaytv.R;

public class StopServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_service);

        ImageView imageView = (ImageView)findViewById(R.id.link_img);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                marketLaunch.setData(Uri.parse("market://details?id=com.innovalic.finalfreetv"));
                startActivity(marketLaunch);
                //"http://market.android.com/details?id=com.innovalic.finalfreetv"
            }
        });
    }
}
