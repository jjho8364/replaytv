package com.innovalic.replaytv.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-11.
 */
public class GetMidListArr {
    private String TAG = "GetMidListArr - ";
    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title){
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();

        Log.d(TAG, title);
        switch (title){
            case "슈퍼걸 시즌1" :
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v8a8apPEkpT9SkTpRmSDXmF"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v34b6OBOMOxD22UIxrM3YNr"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v38b9y4q7ykaP74ylx5QPnB"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5692LkwmwF4wHbwbLaAHlH"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v16a7euTeed77ucdBPTP2Po"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6801YpuKYYfGrWYAGxuGbW"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va6aeR9sGz9sD4OGDOREQoC"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6467KyRKRKKAO2JXZgzRyZ"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=veee5qYajjyvYXSXbqaSZYa"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v71cfD3m5TRTXvMvLTDXML3"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v86ccsgEgM8qZhZhLg9mLm9"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc46cBn3BB43dIkIdcqncqI"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v99b78Z8OyFlY2O28X8ZJp8"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v3a1aZp3EpXh4YGCRRxpAOA"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2df8kvGHpkGT8wHFkmjvuv"));
                listArr.add(new ListViewItem2("슈퍼걸 시즌 1 - 16화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc609mLI899ImLPjI9ejmwV"));
                break;
            case "왕좌의게임 시즌1" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 01화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/08/20110419061702191y6kuvu3ga5e4i.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 02화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/21/201104261739291623nuw5fyws9h61.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 03화", "http://183.110.26.75/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/48/20110503183102650juo1fgrzjpsy5.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 04화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/96/20110510122150413r9utyer6zyfkn.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 05화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/63/20110518055623793kmslgue0j8zrc.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 06화", "http://183.110.25.111/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/47/20110524182243265yt6mcf3ad07gx.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 07화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/54/201105311825039333fah4ecxp4qkv.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 08화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/20110608191007020h3f5c8pthc6r1.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 09화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/97/2011061406140660710o1z59uthnp9.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌1 - 10화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20110621182400509uce670nxjymcn.flv"));
                break;

            case "왕좌의게임 시즌2" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 01화", "http://183.110.26.50/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/90/20120405064025608jocsrynjm0l8f.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 02화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/73/201204061014020149153ziu8uu0s1.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 03화", "http://183.110.25.103/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/201204172153016403lmrs6pqedu2i.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 04화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20120425100244011dpvg13b3ursjg.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 05화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/81/20120501193127191p1zn145wlgeem.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 06화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20120509015526746d3mytk5mok0bh.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 07화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20120515213049463549rqpzk90ebq.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 08화", "http://183.110.25.110/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/63/20120523091458487kq7m2ia4v8k4y.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 09화", "http://183.110.25.113/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/97/20120531062053045vrlzxrgsqs04t.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌2 - 10화 완결", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/88/20120605214448344yj0efxborthux.flv"));
                break;

            case "왕좌의게임 시즌3" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 01화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/40/201304012011460532ilj8hgzs9buf.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 02화", "http://183.110.25.113/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/56/201304090345220030caes6ade6wde.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 03화", "http://183.110.25.110/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/92/20130415204528318mqxk39ji9ndp5.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 04화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/89/20130422193823156moscbv5srjswb.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 05화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/21/20130429184907281awm0ogq9l2u64.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 06화", "http://drama.cd/files/videos/2015/04/18/1429358801c2859-sd.mp4"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 07화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/01/20130513190944032skinzx35ls3c1.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 08화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/2013052021170224885fydlq1y0nyh.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 09화", "http://183.110.26.45/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20130603173309235zixgz4ar5depe.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌3 - 10화 완결", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/95/20130610224605666r2vy4f7u5lq1r.flv"));
                break;

            case "왕좌의게임 시즌4" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 01화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/05/20140407202018299swzmc1r0ntfgk.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 02화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/20140414195530578i17x8one4rlzo.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 03화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/76/20140421185806714a46rldozibk70.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 04화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/47/20140428200150067m7005njtxorxq.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 05화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/47/201405051842025296majj93aoz8jh.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 06화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/47/20140512182431160ake01alw0horv.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 07화", "http://183.110.25.102/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/72/20140519220425592a3qsnsh6bbclq.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 08화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/89/201406021728091135re3n8hmizroe.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 09화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/47/20140609155931640oxcnp1679sj0a.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌4 - 10화 완결", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20140616171244343jlotxp21d7x18.flv"));
                break;

            case "왕좌의게임 시즌5" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 01화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/36/201504131555040311orklm5mgbux2.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 02화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/58/2015081315481339244hmswcsm8nmv.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/94/20150815134440592p89wam8p38nek.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 04화", "http://183.110.26.47/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/20150816150420329mi27la3ihqdcj.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 05화", "http://183.110.26.47/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/20150817122143662pfazi984i1uhz.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 06화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/2015051820475339077cr00ckrj7d7.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20150525215014683padcn5qb0j5m3.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 08화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/38/201508202150327691ygtud1520y7k.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 09화", "http://183.110.26.50/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/201508210627590673lqp8udswls9g.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌5 - 10화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/74/20150825181849630qoqh4t4uknnhv.flv"));
                break;

            case "왕좌의게임 시즌6" :
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/201604252223182958mvzhg9ymytg2.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/45/201605022216489362qugav24yazy7.flv?key1=46323730323535313134313330313731314530373431364433414341&key2=70E43BB24B3393D3037AB5833CE703&ft=FC&class=normal&country=KR&pcode2=70307"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 03화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/69/201605101013127849f11ti6l5l67j.flv?key1=30373845303732324634334430314131313230374331364534324135&key2=CEB8E77995FEB98CCAD8083CE2BFB4&ft=FC&class=normal&country=KR&pcode2=19016"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 04화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/26/20160516183749005azttb55dz1ryw.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/89/20160523223943975nle5jmpxknclq.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/67/2016053022115952106v83w3f97kmq.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 07화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/20160606185232087ualfxg61t7rxe.flv?key1=30423841313533394534363130313231314230373831364631444237&key2=568BC384C31AF2CE228886BAB5855D&ft=FC&class=normal&country=KR&pcode2=78464"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/35/20160613203558457iwlm2jareqwd9.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 09화", "http://183.110.25.102/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/87/201606202235431867m4vn28js7slk.flv"));
                listArr.add(new ListViewItem2("왕좌의게임 시즌6 - 10화", "http://183.110.25.110/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/21/20160627221701320bk3zq5lc4w42p.flv"));
                break;
            case "워킹데드 시즌1" :
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 01화", "http://drama.cd/files/videos/2015/04/19/1429455816f03d8-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 02화", "http://drama.cd/files/videos/2015/04/19/1429455855adb11-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 03화", "http://drama.cd/files/videos/2015/04/19/1429455907dc22f-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 04화", "http://drama.cd/files/videos/2015/04/19/14294559507fab3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 05화", "http://drama.cd/files/videos/2015/04/19/14294560304b00f-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌1 - 06화 완결", "http://drama.cd/files/videos/2015/04/19/1429456071dfd52-sd.mp4"));
                break;
            case "워킹데드 시즌2" :
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 01화", "http://drama.cd/files/videos/2015/04/20/14295370438aa0f-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 02화", "http://drama.cd/files/videos/2015/04/20/14295371815ae57-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 03화", "http://drama.cd/files/videos/2015/04/20/1429537358d0aab-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 04화", "http://drama.cd/files/videos/2015/04/20/14295374896bc13-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 05화", "http://drama.cd/files/videos/2015/04/20/142953758476507-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 06화", "http://drama.cd/files/videos/2015/04/20/1429537676417c3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 07화", "http://drama.cd/files/videos/2015/04/20/1429537841e2eba-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 08화", "http://drama.cd/files/videos/2015/04/20/142953799331167-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 09화", "http://drama.cd/files/videos/2015/04/20/1429538151cc5f0-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 10화", "http://drama.cd/files/videos/2015/04/20/1429538355dec26-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 11화", "http://drama.cd/files/videos/2015/04/20/1429538452f6bfc-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 12화", "http://drama.cd/files/videos/2015/04/20/1429538581e124c-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌2 - 13화 완결", "http://drama.cd/files/videos/2015/04/20/142953868617dad-sd.mp4"));

                break;
            case "워킹데드 시즌3" :
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 01화", "http://drama.cd/files/videos/2015/04/21/142957855523339-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 02화", "http://drama.cd/files/videos/2015/04/21/1429578590f6fcd-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 03화", "http://drama.cd/files/videos/2015/04/21/142957862589db3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 04화", "http://drama.cd/files/videos/2015/04/21/1429578664bbf9e-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 05화", "http://drama.cd/files/videos/2015/04/21/1429578699495e3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 06화", "http://drama.cd/files/videos/2015/04/21/14295787346b127-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 07화", "http://drama.cd/files/videos/2015/04/21/1429578769840f3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 08화", "http://drama.cd/files/videos/2015/04/21/142957880501be4-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 09화", "http://drama.cd/files/videos/2015/04/21/14295788401ce5d-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 10화", "http://drama.cd/files/videos/2015/04/21/142957887675b8e-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 11화", "http://drama.cd/files/videos/2015/04/21/14295789215e6ba-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 12화", "http://drama.cd/files/videos/2015/04/21/1429578956452b4-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 13화", "http://drama.cd/files/videos/2015/04/21/142957899268fb3-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 14화", "http://drama.cd/files/videos/2015/04/21/1429579027b4798-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 15화", "http://drama.cd/files/videos/2015/04/21/142957906379d38-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌3 - 16화 완결", "http://drama.cd/files/videos/2015/04/21/14295790992a5fb-sd.mp4"));
                break;
            case "워킹데드 시즌4" :
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 01화", "http://drama.cd/files/videos/2015/04/21/1429615898f5f61-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 02화", "http://drama.cd/files/videos/2015/04/21/1429615947e2361-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 03화", "http://drama.cd/files/videos/2015/04/21/14296159946e1b4-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 04화", "http://drama.cd/files/videos/2015/04/21/142961604234fcb-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 05화", "http://drama.cd/files/videos/2015/04/21/1429616115b032b-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 06화", "http://drama.cd/files/videos/2015/04/21/142961620891100-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 07화", "http://drama.cd/files/videos/2015/04/21/1429618203faf06-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 08화", "http://drama.cd/files/videos/2015/04/21/1429618341aea5a-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 09화", "http://drama.cd/files/videos/2015/04/21/142961841638ea1-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 10화", "http://drama.cd/files/videos/2015/04/21/1429618518bb9c2-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 11화", "http://drama.cd/files/videos/2015/04/21/14296186589800c-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 12화", "http://drama.cd/files/videos/2015/04/21/1429618806b947d-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 13화", "http://drama.cd/files/videos/2015/04/21/1429618888c4e6c-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 14화", "http://drama.cd/files/videos/2015/04/21/14296190192f5a5-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 15화", "http://drama.cd/files/videos/2015/04/21/1429619109ea4a8-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌4 - 16화 완결", "http://drama.cd/files/videos/2015/04/21/14296192365fa23-sd.mp4"));


                break;
            case "워킹데드 시즌5" :
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 01화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/00/20150430163441468e9mih4xof9q5a.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 02화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20150430163701984a42nud7b7vt0j.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 03화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/23/20150430163841453kd474c6jpueff.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 04화", "http://183.110.25.97/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/38/20150430164058781dxatbjqlh6arl.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 05화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/46/20150430164318750xnasd1yl9ce8r.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 06화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/57/20150430164541234hk9uvbfhf2oij.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 07화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/62/20150430164755937r9gvkv4id6vw9.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 08화", "http://drama.cd/files/videos/2015/04/22/1429667182f41c5-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 09화", "http://drama.cd/files/videos/2015/04/22/142966721732cdf-sd.mp4"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 10화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/99/201504301656155627e29kss4v14u2.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 11화", "http://183.110.25.101/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/12/201504301658257652e3t85q5jp1uw.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 12화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/26/201504301700494212p9fkd7rhnqwe.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 13화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/20150430170316734jstb3fhv3ylqk.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 14화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/56/20150430170546546jntycus83e2hp.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 15화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/20150430170801578yk6asx6ak2y8n.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌5 - 16화 완결", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/00/20150430171121812yqyf0jo6pkzvw.flv"));
                break;
            case "워킹데드 시즌6" :
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 01화", "http://183.110.25.97/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/32/201510140652473228zjvyf4uwlqbo.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 02화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20151020183800789wr94hijelnh16.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 03화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/81/20151027185354039sqa0fe6pocg45.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 04화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20151104070632608vdu8likw48z1q.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 05화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/00/20151110214220092bfc6kg4vnogjg.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 06화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20151118102155843mzti9ysu30wq5.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 07화", "http://183.110.25.113/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/67/201511241843224280gchd9p6qpnk1.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 08화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/28/20151203072251137th8nw1ohxe743.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 09화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/86/2016021618270305434y3zq0fg98sa.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 10화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/13/201602231841567287u1rn015xfrmd.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 11화", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/55/20160301001316247oa76y7fwd99zb.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 12화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/20160308055646150s6kcarut4fnnt.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 13화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/57/20160315181337598a5wux574il665.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 14화", "http://183.110.26.50/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/20160322185426415y964tgto9dpic.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 15화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20160329061305048odx8cs2jdi90l.flv"));
                listArr.add(new ListViewItem2("워킹데스 시즌6 - 16화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/66/20160406065958599208uuqb7d52ma.flv"));
                break;
            case "굿 와이프 시즌1" :
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 01화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage2/m2_video/2009/09/26/2589340.mp4?px-bps=1371495&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 02화", "http://61.110.244.47/redirect/cmvs.mgoon.com/storage4/m2_video/2009/10/02/2602583.mp4?px-bps=1356898&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 03화", "http://183.110.26.90/redirect/cmvs.mgoon.com/storage2/m2_video/2009/10/10/2616756.mp4?px-bps=1369888&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 04화", "http://183.110.26.86/redirect/cmvs.mgoon.com/storage1/m2_video/2009/10/18/2633971.mp4?px-bps=1402165&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 05화", "http://183.110.26.86/redirect/cmvs.mgoon.com/storage1/m2_video/2009/10/18/2633971.mp4?px-bps=1402165&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 06화", "http://183.110.26.93/redirect/cmvs.mgoon.com/storage4/m2_video/2009/11/14/2695908.mp4?px-bps=1366479&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 07화", "http://183.110.26.91/redirect/cmvs.mgoon.com/storage3/m2_video/2009/11/22/2715387.mp4?px-bps=1362222&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 08화", "http://183.110.26.87/redirect/cmvs.mgoon.com/storage2/m2_video/2009/11/24/2720977.mp4?px-bps=1367689&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 09화", "http://183.110.26.97/redirect/cmvs.mgoon.com/storage2/m2_video/2009/11/28/2730672.mp4?px-bps=1368238&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 10화", "http://183.110.26.96/redirect/cmvs.mgoon.com/storage3/m2_video/2009/12/19/2782362.mp4?px-bps=1364719&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 11화", "http://183.110.26.89/redirect/cmvs.mgoon.com/storage2/m2_video/2010/01/09/2839783.mp4?px-bps=1369000&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 12화", "http://183.110.26.89/redirect/cmvs.mgoon.com/storage2/m2_video/2010/01/16/2860783.mp4?px-bps=1367751&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 13화", "http://61.110.243.200/redirect/cmvs.mgoon.com/storage3/m2_video/2010/02/09/2933130.mp4?px-bps=1407229&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 14화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/12/2943766.mp4?px-bps=1360938&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 15화", "http://61.110.243.226/redirect/cmvs.mgoon.com/storage2/m2_video/2010/03/06/3007042.mp4?px-bps=1368750&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 16화", "http://61.110.243.226/redirect/cmvs.mgoon.com/storage3/m2_video/2010/03/12/3024017.mp4?px-bps=1372687&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 17화", "http://14.0.68.246/redirect/cmvs.mgoon.com/storage4/m2_video/2010/03/19/3043048.mp4?px-bps=1365807&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 18화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage1/m2_video/2010/04/10/3101309.mp4?px-bps=1371606&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 19화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage4/m2_video/2010/05/02/3156836.mp4?px-bps=1364109&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 20화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage2/m2_video/2010/05/09/3176575.mp4?px-bps=1360368&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 21화", "http://183.110.26.90/redirect/cmvs.mgoon.com/storage4/m2_video/2010/05/16/3194440.mp4?px-bps=1365756&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 22화", "http://183.110.26.91/redirect/cmvs.mgoon.com/storage2/m2_video/2010/05/21/3207261.mp4?px-bps=1367913&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌1 - 23화", "http://183.110.26.87/redirect/cmvs.mgoon.com/storage2/m2_video/2010/05/27/3224252.mp4?px-bps=1377048&px-bufahead=10"));


                break;
            case "굿 와이프 시즌2" :
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 01화", "http://183.110.26.95/redirect/cmvs.mgoon.com/storage2/m2_video/2010/10/03/4157420.mp4?px-bps=1403185&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 02화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage4/m2_video/2010/10/09/4168199.mp4?px-bps=1368723&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 03화", "http://14.0.70.103/redirect/cmvs.mgoon.com/storage4/m2_video/2010/10/14/4176304.mp4?px-bps=1400788&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 04화", "http://cmvs.mgoon.com/storage1/m2_video/2010/10/21/4188446.mp4?px-bps=1380480&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 05화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage2/m2_video/2010/10/29/4202303.mp4?px-bps=1370572&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 06화", "http://14.0.68.248/redirect/cmvs.mgoon.com/storage2/m2_video/2010/11/12/4224834.mp4?px-bps=1407729&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 07화", "http://61.110.244.9/redirect/cmvs.mgoon.com/storage4/m2_video/2010/11/19/4236846.mp4?px-bps=1301943&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 08화", "http://61.110.243.225/redirect/cmvs.mgoon.com/storage2/m2_video/2010/11/25/4246136.mp4?px-bps=1405162&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 09화", "http://61.110.244.47/redirect/cmvs.mgoon.com/storage1/m2_video/2010/12/16/4277103.mp4?px-bps=1393329&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 10화", "http://61.110.243.225/redirect/cmvs.mgoon.com/storage2/m2_video/2011/01/15/4317340.mp4?px-bps=1387299&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 11화", "http://cmvs.mgoon.com/storage1/m2_video/2011/01/21/4326845.mp4?px-bps=1402564&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 12화", "http://61.110.244.49/redirect/cmvs.mgoon.com/storage3/m2_video/2011/02/05/4344503.mp4?px-bps=1389259&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 13화", "http://cmvs.mgoon.com/storage2/m2_video/2011/02/12/4354080.mp4?px-bps=1396633&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 14화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage4/m2_video/2011/02/20/4366548.mp4?px-bps=1397412&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 15화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage2/m2_video/2011/02/28/4378712.mp4?px-bps=1404583&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 16화", "http://14.0.68.244/redirect/cmvs.mgoon.com/storage1/m2_video/2011/03/04/4385478.mp4?px-bps=1404415&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 17화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage3/m2_video/2011/03/25/4419203.mp4?px-bps=1360639&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 18화", "http://14.0.68.248/redirect/cmvs.mgoon.com/storage2/m2_video/2011/04/01/4430564.mp4?px-bps=1399846&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 19화", "http://14.0.70.139/redirect/cmvs.mgoon.com/storage3/m2_video/2011/04/08/4442089.mp4?px-bps=1399962&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 20화", "http://14.0.68.246/redirect/cmvs.mgoon.com/storage4/m2_video/2011/04/15/4453660.mp4?px-bps=1395754&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 21화", "http://cmvs.mgoon.com/storage3/m2_video/2011/05/05/4485566.mp4?px-bps=1396540&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 22화", "http://183.110.26.93/redirect/cmvs.mgoon.com/storage4/m2_video/2011/05/12/4497030.mp4?px-bps=1402345&px-bufahead=10"));
                listArr.add(new ListViewItem2("굿 와이프 시즌2 - 23화 완결", "http://183.110.26.96/redirect/cmvs.mgoon.com/storage2/m2_video/2011/05/20/4510300.mp4?px-bps=1356907&px-bufahead=10"));
                break;
            case "굿 와이프 시즌3" :
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 01화", "https://d1wst0behutosd.cloudfront.net/videos/2357981.og2.mp4?Expires=1468752199&Signature=Gvd1YgGMuJz6SjdEnulq5bUy55RjdpHLZFrqTPkm7gSYBBvT8PcjzLzKpDfc-46jmZId3ToBY3B~RZajEv4UkHfmMHwxkV560b6om-SXYxEU05sneADQmWVDV0jf~fAe4Lo645qDY1YAAAVaUshVENG40t85ZVrVfBR1o4IJCAL57LLQqSA4pSabBNB8ZB3XRa8Ed3zNPEPsrYOOE7VjcvshJF6kpWpCIqMEBF7jKh2q2IZ6CLsRS-u-LS9FmRCcyKE39wE7KnzZxIym3gUGoMBe2q4n~3yIeYWeh~07943rZjZXHDVJhxQ7DQ9ybrBb37ykZNHua7CiKqJAGCL3dA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 02화", "https://d1wst0behutosd.cloudfront.net/videos/2357984.og2.mp4?Expires=1468752249&Signature=cBAyRy7ceo1ijfP88wMJxxzlYMtFEoAPdIPs8SutCA-XDrUChdCYJPqjhgjOr~waInqqBRos4lRDm7kXBOhpZb7FXBOokEDOiVKN8jCUp5bSwqBTI~RCUuE2Eoz-kcKLj54WV1WK5VNOlK16-9TGHV~yr93BBuOe-pHbkQshSUjDBhfPj13KWaJ1vJ2T0KU575PXXRTv-8XdKMWHcWnzfh2MboxaGRMWyQEY6JcnKZRRQl5dkImLgjWqRnwwgR4BlbtW9Q-MLwoZ9sG-k5MlhlrX7Ep-FeTRz7-UOXZ25OpWKNdhEq02XXzRvHPQQf9046Lqzc464cBB8ZgHUC2FFQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 03화", "https://d1wst0behutosd.cloudfront.net/videos/2357987.og2.mp4?Expires=1468752425&Signature=GWXJI9yBVfzT6vRbZeY8aidCIrYxmRTunB5dundDkDKLROP-bLy3h~fFh9pJGPxj-QxLRR5fW629t53dkdNauEXcR55e-ZImnZKusrbljK33z1AxltoLcCXLmBB2NDQ1MmOxnOj-jTaLEym8vTR9IvJWJhvJV1PSjDmnZu6QeNfnr3m5J8bNlkvwSiIDcEyOhpBRY2YXC2aTZ06i1FKkiQ3JRqZ-U9OvBmNiYc-1Oyymr40FWMYiWU~wntAofNQz2SOEzjwfIuYcMHSTHucNEzjXOVDo1vmoK5oi0tCuiaCfiopbOxfHhe7RWN6n57zyqBLvqMUe2LFjZLTGscbPjQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 04화", "https://d1wst0behutosd.cloudfront.net/videos/2357990.og2.mp4?Expires=1468752443&Signature=lKxSUFTPxSEo0lUbU1zDbQPZPwpeIrTQoNmpdTOAL2LEyFWBFCC87Edcwwr7oQ84BuBmfwMEIJCAyF04TGsYhyw36w4HWkXMDCZUmq71NS8N7B6dZ0JwoNb-Q7SO087oCRLb9DfP8w~OMyzm999wP~~Gz98RCoMmD0su8cA43GR64WdtvFsZXnASvIuvBtYUOCVX4c9yhgv-BgEB8msMgaEsiIFPOeZCDDAuJflp5mse1Rq2tE-THRZ1~YEPhlQOVPErdwDglUvNpIjGR7qs8BvfLp3DR4dsQCM7y8YW~WVfAyHJ2JnNUc~coWMmckoiTLOKBDdIaeBHWT-9-kr~eQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 05화", "https://d1wst0behutosd.cloudfront.net/videos/2357993-clip.mp4"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 06화", "https://d1wst0behutosd.cloudfront.net/videos/2357999.og2.mp4?Expires=1468752485&Signature=BcAdxFFMnjXjYKhFFHgSo3ccFl-2V2WczvPsAaA14OM~fnm0UMDMy7602nkoHSKB6NkNMjJWFGGujGblMnYX4818X~pNj3EoV1gXGpH7ha-9RAqcpK2zrQimNPp-lhz9UHY5wjBGKq6aJdmNa1dYNtLgl4VkRdQGNEpF3ufaZFa0Is83Mhzkck~i4s-qUan2Joc41ilCFJJytI7-T1En8FrBtopbuvIjICQ9ZZclat9EY0cZQqZ8OH8DLT7-tevq2qM~8YhWSBezxxrn62O~2Nm62~-xdvLOzhwWmxUcsMaZb976IszkBVBsbT351MsOzNspLUbcPw72J9GhFZBJcA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 07화", "https://d1wst0behutosd.cloudfront.net/videos/2358005.og2.mp4?Expires=1468752580&Signature=WSkvTyZH-Bz8vhZ1pfNUUsK3trcaLW-XqGSvn4eVpDIO7hBk7iaZe9~rrubkCrBofF0E5qh1S~wlu3pF9Z57yHEYiPcHTLO~9xErKuvr2SqZUH-PUlFbeS~OLfA9wTwgz7-hLsN8htKMR5bavT6MDHIbYPB4EbGrPCxk9PJQqxXA4QyDfVVUODzCb2FB2bgcPl43c9Ev0HJsalCiVVKn0bNLAKr0hUDRc6qDDSeLnI-5Bp2pcftEDK37LMnz38UmKqojzWc6o~xVFjE8yCThoYWY3qyrjDZCV59OrIVoGWydHJXDdXX85NUOD4a991m7L4VoSFLAa1dtoxHjnCgCug__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 08화", "https://d1wst0behutosd.cloudfront.net/videos/2358008.og2.mp4?Expires=1468752601&Signature=Z-pFlTDLFd4DStSkL~25AJoVQJABEdiXMVAEZAmx85p10vpQLZ7kyKvJh3c4Vk0sdLHdzcsUBnxjmLPU-CktcvE25LpvsSwalKpez~PrZ7artdUbvrMSYx3pjc23GepmLtgqvOTMcODOTN0Z3mG3tWMkGsDBM7~3oS6UL1Yyy~fJwJtp4KsYbDkigLkd7fMjAGtzV~M5SjdLcRRVtDzmI~SFvYCEGkrFJ4hxQMWmQd9yXBp3NIMdUTcjMB17hBPTgrBNu9Tk4bssY-E1GPyPtsFBK5fcVMpo3DVcQb8-oudJULHb1GEShaj~iNaGiHpZHYN~QSyXTWbvveACZakjsQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 09화", "https://d1wst0behutosd.cloudfront.net/videos/2358521.og2.mp4?Expires=1468752622&Signature=D5rOAJ8N-PDKfwdm8lZ2wgmdb5f0~Ux8~yN0J-yiKvL3o8Id5xGg1NrJuVgpgZyybReoslt78FsdM-WfaVtqPXGlMY2tanQdWlo~ONv3CmFCRpifhQQSRELp0cfHda8K7nsjtSMaNcVrj0bM0ul9mGAKu50SB5uW3e4UFf2zBKmkNICDrTnDnJfcrVM1gKGAd0OhZVGcK~gpRNEeuOAx3G~WmuCYvTny4w19nLl6KSnCGdipjGTP0FeCH9422CWOfudPvZ703b~Cj0t2G~IuTU7DN5wub7u2uTYxQ56jU2~adKtGy4E-ouA0kAuIsKSzd35ajgeLLeNbPkkFqcMfcg__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 10화", "https://d1wst0behutosd.cloudfront.net/videos/2358524.og2.mp4?Expires=1468752639&Signature=LyfBqRF273WvcVVONaML~mHkTzA8V09ilT9SwESkjLRm0VLP-Zb2chrs-b6QhCy3wAtDCkb9vW1P1gNIfn3DGIuB8KNsZHtFdcCaekdUWMUv57zx4zSJLANnLUBAIWgvrvT1DZcG4A5~CDOAkfPGdVuubP5u7vygHFeHYIcO0CZunKLGuKAiB1JMLlnAZR7NicBEfi2lIscyLB0ffg64ce5RZMINT4kEwJDRSf5lGjwVfLmnQSVkAKeOj~b9ePqUiJNHUM4a9XYVBftxvAOD1Hl6M6pDGitQUReMqmlRz2uDw7E1RDUloaYclMrUJAcISKhGKj69VpYwM1CNHNrd~w__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 11화", "https://d1wst0behutosd.cloudfront.net/videos/2358527.og2.mp4?Expires=1468752655&Signature=icJcs3YKxa9ldBzPNeAGYy9Qcxw9fv7AHjRFflaMxPddXeWBBiLppBCJogYewAicl-CZyH~-F0apwubd88Ixs-BgmUy4TgmB5M0Acxa-WLDhl4Dp7RmfCt4iLhFBvbTvti13x0aoCEVPvAZXzZ5zglFDmFDY4o72FWRzwQu0Hh5xMnJmc6qKSNw51jBBCs32nDUnn1ks-UCzJQFu5P6G3njF~D65lwLpmmXmqTkV8TyprDuB4Yop78J10L4NbsUwrR2hH6x8KKnqcVKPz4qb6WKmLpN09h0II4~zL~kKKGap6rBJyReWDWaFbJJkLm4A5sDj85ltkw4AR8RJ8h97uA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 12화", "https://d1wst0behutosd.cloudfront.net/videos/2358545.og2.mp4?Expires=1468752672&Signature=oSzMmkmwlHkp3ArRUOGtJNr9l5nC5Awa7TtxM2ZkTBaoxQ4ROjc3mCFjWkZbJFNt4GOLiDZBY-4PJfSQ3DICZrmDTNz94mw2JtGxSfZjR7kSl8tG5maQ6P9ElLSQj7GZHa7XgYxrgR6gP9jhpJMV8I1G~htooZaBIJW2hByBtU6E2VYZkI0LCN8PxBkAse-P902qIoZHwqFcDD~hMMicnQoiyjZBDVfU~PVi~bJc84QnGe7oZWIRRFqUUcSMeLf6F0kapP2tcIWy5Jg9HmVxsQzbWpC0cTwJ6-CJWz1kRJCkQc7xu4caSCWuk93V60iQAIe0YJ6VcSH8D3MPUBdrTw__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 13화", "https://d1wst0behutosd.cloudfront.net/videos/2358530.og2.mp4?Expires=1468752688&Signature=cqCqLYfJ0CoAnbZdS1SiE2VBYhhJ-rL3J2~BzSRORCj98ovZ2oY~Ugm9FF067gtgbwuy0wEJEYLIXeKFIxXOuym1ornrZj~NJnwWglpA1cI1jo56tvd~GpOLTOgtdcdt~YQEVauttv-HbRKgeV0Q2U7O5x7MJZmUkta7CsD9hC~ATiTo6EfrZvBinj8W7CQRbJoT276rh0t86VjABiWijk~AzAo2XWTt2ABqkk2FeQO4kYYmFAvFoHbShgVemXCwRlf~Yh6CZ1~IFKHpOOlifuxTzUNUdRmRz2g-B5iH0PeRrNu~fgPsaT4ptlJktSGbXOme2LRFD6SZqrTag6S6OA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 14화", "https://d1wst0behutosd.cloudfront.net/videos/2358533.og2.mp4?Expires=1468752706&Signature=OMnjdLUz7TrRpmtDgLa41j9A87E2zV~AbSZnRISrbIAPSh14np-zKV2j2ckLqr3u3ubJQxcyWG67kltfXS3l6wUlXrXsKBmQFo9P78xx5jxM0Ijyl69G3efsTXXaUqPZUj902U3YEVsPYdzbnzPNguNMm02PAGX73N~AgGYg-E1CEkigIUpyddUFS6psKgcVI81R8ZcOHS-6cUzoADrN8TCe9sYgfcnVi5o-ziNZg1bv9S3jF~WzcN0CfY8D5BvKvqtP-fuxp5a0ksF0~-5lmSTwlzCVfVoaPwZlegtDImy1wp3PdwlUgkk~TkOmKi5YY-PHNOI7dmBlYR9dut6POA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 15화", "https://d1wst0behutosd.cloudfront.net/videos/2358536.og2.mp4?Expires=1468752723&Signature=Xnaijly7uRjwzjpmPzCoePJ-29J4APIPIdSnsq1vYNXv5~69SDsmQMBp24VhB2ZD5aVi9KHKH7ZkSy85qvMGTwLogOYBYliqZVfpPhrWCGDIImnYSutKSBUDvZfS6fwdchxIo7~Y4IDeaoNqtLd-bC8sMdNqPpXOpvbPi6pEwOoBc7CTD9UYun3o-90n9GvNEkQdUrMsHGgxntzTGya0F59NX4sBj231iNir8M~MiFOJ7iVrC8dGeJ20xejdvOJLbo2OfJM8YmfiUANmsMmSuzVIw1S9juZ0UaoVEvUYQ3uoJGBlHRli9bTHXMjIu4jEW4tJ3EVfff6ZHYqHLbC16Q__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 16화", "https://d1wst0behutosd.cloudfront.net/videos/2358539.og2.mp4?Expires=1468752764&Signature=NU~2T4UsAxcawald-Rm1TucP9~DuD6BnsemgT6PB-7qQM0EkrrswaHMSckoSqB22NtaPokU2GhywjuemBbgPt-6Ujg1eIA6MCnOShPJeVNetOtuBM~dxvpI5NzlMuYjVHSromN~QczNZNM3bKPvfEJOkM~gpL~xoiJcc0q2TyREKKwwtFH4V1cZewzQDTYsPeTUUMajB9RP~VOSAD3jNXGJJ0y~HJhh9GOjC9d7npnfsFfWb8BGakZRmG2TSrAISIhTVAaWA9nhPpQDbxO70kLlXSCK7MqYQmqQU~T~UXCdc~MgbrIcEU0ajPN3FpYzrTezaKcyOfgtvv6n8nov03A__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 17화", "https://d1wst0behutosd.cloudfront.net/videos/2360390.og2.mp4?Expires=1468752784&Signature=Xf5okuuWh5ARZ0-7z4OAboNceVm1GzHhdfZlUCNKFnCKLaO7oMFJG84zVTWvXS269mTZHVPt5VKTsERBkaQbtIVdUt5uLky-9ZDk68j7Uwmixiy0xKkjAIY4nYZn4HLKFTIPOEaauATbfQIkOxMQb4tijl6rlxlHvIGb-ktEDAp72xyfqvqyTmAUYlIISuVGfMRdCs-YQZlcYWiluU16W9q7n1Os5-ga1rGjTQxLtH~a77ZgaxeL9x6wqACI-kVmMKBVNjXiLBPu33NFJcTnJVtt-9-xxXb7wQsJxyOeaw10Yso40GPyJezwenEt9RKRVl~7nT2RJI5rrv5R8v-FkQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 18화", "https://d1wst0behutosd.cloudfront.net/videos/2360396.og2.mp4?Expires=1468752809&Signature=GON3FHZZyfDgtsZ0WuRxKwo00QpKITFbuN5vSrZO~LNDlUen~thBbN0QvEHSXTSDz73300phfZkkFA3VntU0gM6VGw2SMQLq5KLWEheEf1ENbL3qD9caOVeZzqf2nxMNzUd0QQRZ0UtQ8CJjcNsGuRQr~AbfHHUv~tMgZlcbKlljPgV23XbMagKYGXIi-kf36Hz-r51~q1qYAkVHTsKip5rN5WmTD0QBLH2jvyL7XXcJn7-By5bkWXFWjPNhcSRqjMGH2qqmkRRSg37I1cKlmwzn1faSmPC1~9F0eMJbRKlaMU2kb30I7sPOl8EPSzvhRVw5f5kfoomLLod0MhJH7w__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 19화", "https://d1wst0behutosd.cloudfront.net/videos/2360399.og2.mp4?Expires=1468752830&Signature=GKQDejdHDJuPyrtYq-TNGBnDxxlEY2oTL3TVmTFw~dgvC7d2c4-D9lhQbR0faByqlIt5OZ3ehBZ95oMXqTAAN4-YJgahDheV8brkk4gqlfDtWEOlq09gpCPu11zNmI1WrLOgNYKiE512EbHmcNumaOpLxl~GOPE6l4N2NDqwPx1RcbOmg~O9Thnu-xRy7fNUMPVy5qP7~pN6xa39glsU3GeP2jrn5XenSw16~VepFk7NBuLKHaXKETjb4yMx2Io5Gh6ACUS0T7mNCNrkpdAOmL8b4LBkXqQ964ZPuUwM0UJaixjK-i3oc-~g9mXolX1VsrOUXXo11DWVtH~0lic7PQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 20화", "https://d1wst0behutosd.cloudfront.net/videos/2361458.og2.mp4?Expires=1468752846&Signature=iuKGEFsBvzGWo73hpUEOXqnGKLgd0rMxHbhsX4JHSufl0lcEAaHlaAK2vd9Y7Ddq0uswGj8czFePBve~ef1bHUh275WlDymKKQatDDpdEHxUUExM2s4KKhVEMniujeDCFdpE1SXC3tizrCjhu7JcjL2cqRrB6O8WC3yDHgjvyXkEZQw8pSLzhMGj8F3Ar77s53Gxj4AU2pkiz9GdwokVJOByL70qJE3~bKNQMXj1NwlSZT-YiY7cZFQ59fLS7U~7vlF0P~b~gp2BIc-G6NEcw2Hj~war6yQzmiIvniRJTZYbcaoXMNCHe2O4MbB4VOr2TuZObn48~zWEsk9Ela8HBA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 21화", "https://d1wst0behutosd.cloudfront.net/videos/2361464.og2.mp4?Expires=1468752862&Signature=GxWIXfbQuoAMqS~TH51dbbZ9RGsa9fTz73uTk0WZwHNcHaMoL6AYhcb5PR2WVWkvR~39R4yOcamz3wzGJgpIyYJB6SfFBnyPSOJj~7WdHdewelrLTYskTfuP3WduIoSFDVGjorGka2nPNcfUbXYsAYAyYWgjvTSDZHAKuFGj3CGmG8BEUVWLXCd0qSqH7BdwOcSYQQpth9ULQ41X7qqfL1-Kyiw3yTxtCaAQMjYPyKxgpbiJaS7hckxz91P3X-Gkvlu9V-b7C2vnKl8YrgFZh1h5-OLY-gM23biZ2ngQc92f5c3M1NVqRXYRpD535sUaMV3q4x6w3Z2a48HyT2OPzQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("굿 와이프 시즌3 - 22화 완결", "https://d1wst0behutosd.cloudfront.net/videos/2361467.og2.mp4?Expires=1468752878&Signature=lifymxqEgsuqLmqBVfa134M00DGxx6P4an4-1vR--4A8y4sQI1t-zp7iCWkMjCMsPsNR7IavulGV7gpCdRAE5lYvkAQh7I0rcSNXGyzk79qQi8iBddiR74quR5wvxJgxs0M0aHIVbPgiXz7ImVJBsw~i5kOLi3eox5R5FuoHhmeO4IgBAdwZ5rplP7roFl-VcP03tvMCNzkHXftuHnnEnfpnyeDZ-nshYG7qb3pM3jYHZ3kjnD8seO0GsDPumw9iPU4uOxUmibWWbAKfDPdhxyAcPAOP6Z~59XEdxj6qYyRvIJU~0yRnZBidmEjqE~ntl9CAxtINktNlSajKuqv2rQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                break;
            case "굿 와이프 시즌4" :
                listArr.add(new ListViewItem2("굿 와이프 시즌4 - 01화", ""));
                break;
            case "굿 와이프 시즌5" :
                listArr.add(new ListViewItem2("굿 와이프 시즌5 - 01화", ""));
                break;
            case "굿 와이프 시즌6" :
                listArr.add(new ListViewItem2("굿 와이프 시즌6 - 01화", ""));
                break;
            case "굿 와이프 시즌7" :
                listArr.add(new ListViewItem2("굿 와이프 시즌7 - 01화", ""));
                break;
            case "굿 와이프 시즌8" :
                listArr.add(new ListViewItem2("굿 와이프 시즌8 - 01화", ""));
                break;
            case "하우스 오브 카드 시즌1" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 01화", "http://drama.cd/files/videos/2015/04/24/142983730032ae7-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 02화", "http://drama.cd/files/videos/2015/04/24/1429837428f5fb5-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 03화", "http://drama.cd/files/videos/2015/04/24/14298375356a681-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 04화", "http://drama.cd/files/videos/2015/04/24/142983765624199-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 05화", "http://drama.cd/files/videos/2015/04/24/1429837751f2eee-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 06화", "http://drama.cd/files/videos/2015/04/24/14298378391e26b-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 07화", "http://drama.cd/files/videos/2015/04/24/142983795296f3d-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 08화", "http://drama.cd/files/videos/2015/04/24/142983803708f09-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 09화", "http://drama.cd/files/videos/2015/04/24/1429838139cad56-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 10화", "http://drama.cd/files/videos/2015/04/24/1429838231a14fc-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 11화", "http://drama.cd/files/videos/2015/04/24/14298383323f4bb-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 12화", "http://drama.cd/files/videos/2015/04/24/14298384199b94a-sd.mp4"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌1 - 13화 완결", "http://drama.cd/files/videos/2015/04/24/1429838550df653-sd.mp4"));
                break;
            case "하우스 오브 카드 시즌2" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 01화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20140217163402787g90i1waltzhyr.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 02화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20140221151728927zepf6bdtg3msp.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 03화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/49/201402231624337546uf63wusd1z0q.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 04화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20140224234631203jebw6s6ivd6vq.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 05화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20140225161625972sznqwyn9cvy1d.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 06화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/64/20140226160913597huufts3bw7lug.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 07화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/83/20140227100843988n05emjbn4l4c2.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 08화", "http://183.110.25.102/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/69/20140228181056894tu02urz49ax64.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 09화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/59/20140301115333722yczumft62dohj.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 10화", "http://183.110.25.101/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/201403021544278797ywoc4ywlfs08.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 11화", "http://183.110.25.101/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/08/20140304142301769ynlyselh2avb3.flv"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 12화", "http://14.0.94.247/100/148/100-187148-53c8.mp4?_metahint_fn=/100/179/2014/03/06/20/V408735.mp4&type=pseudo&key=iA/CJUNJgrBR5msYNIwaxAg+ByrKxNWFacxffVLsenaS1tGe8wyhdGyou3xcF06NIakCbguJRopurhJTDIkzaQ==&packageId=1000493&videoId=5153200"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌2 - 13화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/09/20140309142334193bf3pdynrwfzxl.flv"));
                break;
            case "하우스 오브 카드 시즌3" :
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 01화", "https://d1wst0behutosd.cloudfront.net/videos/1737319.720p.mp4?Expires=1468500183&Signature=HsTBgBT~UlQTxRzEkfDVfEIK-af8WBpCpfUxP8b10t1WOb4pJnI-FZMAeNLbXcF66XyBpEEufdn0UXYJggkgfkkBxFbSVWsWuyLal4XzUDteON8LSNLOjpsi1uruv7Itnp~WRDreuj-SYsbJNKD1Rq-huaUXUPlqgd3ZZTYlmeLr~7BZJFkVwtsH69j5fYt1jnryrneVcmpNcBFKUZ8fe99ZofWIeEpn5EQPn4d-Eg4TEo939CTB35ozyvtawfTKMlmBwnDVWS6fMiIXGCE9v-ytM7ubqJM5KGAzvuaPpjdXRMFs1tsy-IitPrECRByK56gxfVKyoi9v-qVN7-rx4g__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 02화", "https://d1wst0behutosd.cloudfront.net/videos/1737523.720p.mp4?Expires=1468500245&Signature=ckRDFRRzSkx4q0FYedXwrgveLon2WumE4VZDTcMuUSIppozRlYQCzugAPd2T~wuM8WoG0XVsu0JXTPGVf3Hq2ijnTtCjo~Aw7uwwvqKVSpGxmq~oNZkiqC6o39ypdZQDMyfMfxSJz6zwXZQyVzltLGYaJkCQJvYcf1Wa3fUazHqrKKtIeisjeGhM2WLcLuJW3D4sWlkBraKbmNuXjIPEApz-FTxxNIkkZP8kTQF~srsAdviWwpC~x60befK-YE3bKTy-XIpiRyMlATLRaC~pv5OWB0~8W8EyCU7b8ZjTqylfq-DYECTDcfN2V9T~hZsgmYKy4Jlvw6YmJ8wMGynCFg__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 03화", "https://d1wst0behutosd.cloudfront.net/videos/1737973.720p.mp4?Expires=1468500261&Signature=IdAHBarYmTKcrICot1ZXQQBzPRQTZFaTHllGdQ-3Toasr-~~12YNMILWB0xJ42uwrQdVSfZn0KZfJOel5MxlDgJRA8xumMKV~aAP8N8VrGSxoWvhGiXp4qUmTXJxFvH~zUOJ~VUZTeLRDK7R3ImHHIEnZVpTCzAYJeohLkpEv2nSkBWQUH9oPA09q6pzd6tIgAtMH31FW0GR1gC7pmDW5igIVj5tki12LNDelF8X6hWuenYcCbv1LSeMSqUg4aa6j4La789X3kVf4G4UTIuYmdNEaNaNGjaM7cwlxK2Pk8VxoStfDH2m~2kl0-7nyqHwOYZHNm9XEVfYIE4y3Kg9wA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 04화", "https://d1wst0behutosd.cloudfront.net/videos/1738021.720p.mp4?Expires=1468500281&Signature=X~gIZ7V1z~m~32uPlR5OAfy-7b7tcYLVW2mx5niUwXjYdPp7IaZdqW73K83JMOw~V~upeNBi8NkPTp4qvCujLiC9YcXTQ5Zf~Wk5ggQrAc~WhIGBJ4w79J0OztiAuW5vOT6RbvD8a38ORS1c05YVXYnIfwGxB-aHe1u0ldUuecTXA~xCPd567kETIVB6vQruPXRMeqB-Qkb5KoTstBqu8r5gG3eYpJ47VErvJQgSSffCACL2~ThXqQRojEaAvKzTeOGqJmZAto9TnLysCLZ9I8beFnE02xAvJitNdeHi7JDTH~ukJRft1SE0lG5zmy6zCjIEoE13lWbYTFRXpUFlUg__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 05화", "https://d1wst0behutosd.cloudfront.net/videos/1738081.720p.mp4?Expires=1468500303&Signature=Co08mTXaK3uXzBBSpPIqqwaReegrH42YPODr2Gw41bzgLs~kSmXiYqPKQ79rQn0s5Z1ADMy7W3KbHeOFdNLhX72NqlYypJ5HJSIQNh9MKFO1CBxeRPTSrUWSjUccMUSHPDe7HtMH6BGuGbB6TFxA2y~FRkmLpgUHWw8hTL6WkDyj3INf3T7c6KrzgJuFdPWaUqlbW76u25tvWAaJ55FdsJzYkB6IQHQPMb3R3ow8zC7UJfz6UlbMRRMXI1ZfG1iuqr9V4B9qWhFgbfRLb9jxMqD1YeBpvKlX1Gu~5PkC8EQ22g3IdaTGBfddrg1zsso~wEcuNyu608pIUf0GSyyrNA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 06화", "https://d1wst0behutosd.cloudfront.net/videos/1738309.720p.mp4?Expires=1468500319&Signature=J6DD1GdNspM5dxrR~dxC9DJNOvHcI2Munm-SO1ijUNcGw25F~XhRiJ85fLtttWErjxB-LB3-wE7Zj0LCvI9HWYRQ6brIhMtcvmT9BxLlZs6oTE72~BdxZvuc0KuF-xLKwuRtv-Vw9FiJgR0tSognKFYJUtt~0HX-213FrJ61sh8yJPI9-faxsGE8S2LovdG1ewRJ1jmzh50e3rKduMRYqmrkdoYLLQzRKuP71FIkDJZCCNXasaUZcp-hs3GKdzXzMR~5sM4x~IkXbbEtbJQDdPs2aCDtiSqDWhBI8x3I-Uff-AAbJ1M5U0eemwyAhOHslUTvYp7rT4V22yUcbREhWA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 07화", "https://d1wst0behutosd.cloudfront.net/videos/1738444.720p.mp4?Expires=1468500338&Signature=I6LYdDxSEmefBSPEnoCEhht9c~Ti0B-VcUuY9SRQaWmIsrhZnFr3NNhlZYcNfpi7nIbDS8rHloPiKFuBXs87gOmp4lFYSrLVNaCn~iNHXVakm8X78MXWJSHz9cA01s8apoOzu5PhQplLGUMy2x2QY6qiMoED8HjOj~JvE0g2m-pD7TA4ZNIt-6bHQcLwvmPn9Diw9jXuFt4xhpdJqsL7J0CXmv77s79pShETPwgG7AlgJmZBUIbk30Ml6XV9eWbNUqSlF6DrTqjb2DO7w87CrJU3VRN7iY5XcxL127-1MlgAmjh9Rl2DRhgIOm7FjtPCMZcOjT8rkpeAcA9-mlhH0Q__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 08화", "https://d1wst0behutosd.cloudfront.net/videos/1738240.720p.mp4?Expires=1468500356&Signature=TeIcIspWALmqvRrkCkgZtPK~amPhhDYCIkIaOAOVzVznHqZ4EUb0O~UtPG5hwHDnEryTfb4bemgzJyjR1bx0-xf-JdCqgt7Ev9F0hoyF1lF3aztOadEG6llz~xgfDW02-AE9gvn54zxyTHrT0-cNIpsFhy1d9VPyp2GpLzpBl7D38UXE61fG7hredYxFO4CXs7u2QWTVyK-WmE9HiAvutJd1F-mX3JAN-P6CB7ady49IsNud60QZn~O3B4AHHME2y9R51FJhn2eDdfM8KNeHCLG9C6Ix-v~3cNYhbAK-apZo0-ow7Ke0NImtTjLRS4tD2yFd~GeXnIpgYOvbxVN8Ag__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 09화", "https://d1wst0behutosd.cloudfront.net/videos/1738954.720p.mp4?Expires=1468500375&Signature=l0V8utMJnhjL63HLvby5u64AGecKKiyxpZ3RiYSYslGmvCVWNOqXBuTmZMpJzRgXZlj31TYV1pESl45ovc53YoW30wCRcn3QX7NfAoDvLka8RvqzpxvSNyQx-U4zTjvydMKpU1SWv7-tATNbSCVOm-ve93Wa~5F2n-hUprc4QtRU~BF7L2iVKphaZZhMOA4lMVwz2dtrfiQhsk1eHLiX0GCFgyVWECSqmyYBDpUr2Kn37kSPQVyO8r7Ml6m7WUYHP6gVAhIrpMHxE88DBHwYUryyCMcw3DGxXYTsQ3m8H788MVh4Y5~3MzQBvHqmcLbPfhHwJJWYOOhhehqE64EQEA__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 10화", "https://d1wst0behutosd.cloudfront.net/videos/1738957.720p.mp4?Expires=1468500411&Signature=D~QCHvV8Q-HM5HClddNfcLzyubDPJz2q361cZv3o~gRyTrKQMVBPub0w0-2toCZbELihxSqnccYhQofEZs2ulEnY8Z~xQ9IVTpxh951OgWfC4YqE3SQjVnqe7Vfs~Y383QaOlknePJ4WxRbbiMFqzn-eJ1IHAY9Ir6vox5pWbj96~ObJ6OTd8JT551muTcAWeCoeGANxJkApziXw1ly96fxzktv4dBnUjq8HUMnw40VVgL75L-~KzF6EMEQ5TYu9iFiflF151lBy25k-aPfzvKU~JaKFeBSKD5rY2NXbc6LVCcvDpoyCzuRn74kOrHhxWVujke72IiezjE~LQFK-1A__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 11화", "https://d1wst0behutosd.cloudfront.net/videos/1740199.720p.mp4?Expires=1468500429&Signature=W-LpKxYmHkUoZxREKKS76HQ82tBReNjMSnqL1I0FdsmXR1OyM6peHmjdCasWZxrgrH1gaCi3oTRTwfFzji5-QOOmyna0Wzb3NUt1SKrVSZO3U8iaRd-EwmrmD826tehnOYVS4aQPXwLE~o2ulgcFzz273TTZgDPlx7Z22HFZ5ScCqlSBNxp8Wta4f5RL7i9dp3ztlfeXgEPMlQWQSD~auknBHFAniWNBgv4xTXssrJAQlOvmaoktwwT0fdXgGRjtF65QTyPuX5ZsHtcR7OuNM5cngoSnGPX7f5Z31YShOEH194ZAtr81Q3fRDK7MKcDoA7Oblc1EXaI-U4o0UN5xIg__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 12화", "https://d1wst0behutosd.cloudfront.net/videos/1740205.720p.mp4?Expires=1468500462&Signature=DFU6QFyU3Jc6e6iAuITtjLw74Ey~A3OM~JTv43uZBdI43bPih-J71MeKCkwyBGEWOyIcu8AnPBkVpCnXaw2T-3An1~9qM-CK1oKgT7hooQyXpycCqq0pe1W4rEvyPZgV2zftS9lsNY6B9bSb0WJDZBjs5nfy1C4gagJ~90g7hgUAEUYcDY0jEq28Mr7PYYC4qgAJyzwTE7E9XjGJHRpgXUA69qxMvdff9iehnhEHXGqgCtrZQH~5ZnIUOtl~Cf8Mo-XH-AHpLM3eyvCZmc8I3IKe~6pXnf0IcXiWN~hq2PbYJUNl-M-lJS6CSWoniDzwxF76M2AZQgyH-k2c47Fixw__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("하우스 오브 카드 시즌3 - 13화", "https://d1wst0behutosd.cloudfront.net/videos/1740211.720p.mp4?Expires=1468500489&Signature=j6bX3Q1nBC3tr-FC8LKbcisbpiB2r9aU-9RSq-O8p5upPcQOhR1vZerMJ-HoHQVyNqthP2n41CtPsFu5iTcKx1T1rO~QGpeTlrdlIw-z7Edy-aHLTlPKLnUyEVwFFpXtLESaTT56t3iVrQbVq3B-hNkqNLDAgzrV7m-kMz6RIlzZIBXgeJkkWZRdVeBINrVtmb2SO7EKkieJWW2hqLMEsvLjimkPqrJ3pL5zPyM4LkAA-bnKQvCV49rbjJzBuUUuqFeFSSlIB4DuKcbReTdc4k3lzptZ2CMOggFrXTYu4npILd-ohgYAmlb-Nfm0qDVId49qgx~NaY64azR6ZoKV7g__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                break;
            case "히어로즈 시즌1" :
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=8L5nJOj1x2E$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wshSaiRHPMU$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=HFEcFbcqJDE$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=kEuiuKPAz1E$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=dneKBLI2vSM$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=sStV8nGaNHM$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=3zzRfXeUrrY$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=mFHSSq0YIgs$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=B8z7rO8SfYo$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=HFsnkBXYeDE$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wJIG3yRBP4w$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=KtisrDFW60s$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Q_PN8EWIVqA$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=O-GQiUNTSm8$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=AYFJ7jHaud4$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 16화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=V-XVhZmVqac$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 17화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=icVeB03Vvvo$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 18화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=hiahMPGcyOQ$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 19화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=QrWYvpeZcag$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 20화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ipdHFdKmcis$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 21화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=VuimZb8Kv90$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 22화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Nk3T5DHq7fA$"));
                listArr.add(new ListViewItem2("히어로즈 시즌1 - 23화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Poj-8Rm4CNg$"));
                break;
            case "히어로즈 시즌2" :
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=R_-_XoUOOPY$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=-tNmYFm7YqA$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=e830sOHZwiQ$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=1CyHgfuKFRc$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=-zd2jDO4N1k$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=CKkXBDRCBeY$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=kl3yYh2sMn0$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=3lwajP_goDU$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=_e8pZoglcZU$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=-TUgyLLH8N8$"));
                listArr.add(new ListViewItem2("히어로즈 시즌2 - 11화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vknT_SV88HI$"));
                break;
            case "히어로즈 시즌3" :
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=LqHFktDN1yI$"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 02화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/60/20120915010252054pfg5bdjrpg20i.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 03화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/52/20120915023629882zll3wyp4ofsl6.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 04화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/79/20120915034343226t35xskesdtlra.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 05화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/08/20120915043920257j704qxpbotson.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 06화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/53/20120915053253476okj9tckp8rlaw.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 07화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/86/20120915062832491pe8kbi6k7m60y.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 08화", "http://183.110.25.110/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/43/20120915072014132l3uvtrj2gq3hp.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 09화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/61/20120915080935460tcvxnfl2jajx9.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 10화", "http://183.110.26.47/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/72/20120915085920554v2iecgw71b821.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 11화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/49/2012091510004997677r95e93b66gl.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 12화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/23/20120915111011585uhleeh8isnrx4.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 13화", "http://183.110.25.111/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/30/20120915121435398yjqozih6wr8fg.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 14화", "http://183.110.25.103/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/97/20120915131631616lk0g1x0ap6cvn.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 15화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/36/20120915141636835arnvnj2ggdv0v.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 16화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/60/20121208165441551auy5x33oo05ie.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 17화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/75/20120915173035366nnrooahcolw25.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 18화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/18/20120915183340023gaiajxuptrae3.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 19화", "http://183.110.25.101/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/02/20120915194330851zhxgyph572y4d.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 20화", "http://183.110.25.113/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/68/20120915205009366rmvyd1tdrxlyh.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 21화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=DBPMdBlmIEQ$"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 22화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=BMoXQSmedwU$"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 23화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/74/201209160112174138u269hje39pkh.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 24화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/53/20120916022001023f5ppzq2n8vx5y.flv"));
                listArr.add(new ListViewItem2("히어로즈 시즌3 - 25화 완결", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/r/o/rose9415/02/201209160317310385luq26s4lycet.flv"));
                break;
            case "히어로즈 시즌4" :
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=a44W1wVKvZ8$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=QjH_2beMjYI$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=EKGVLM4ZoX0$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=oKhUlEpibUE$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=6Wu9ox01uM8$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=epjrWD5rBYs$"));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 07화", ""));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=_Zf2Cy-F7D4$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ECEv6F21BPE$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=G-h6jGA3K-o$"));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 11화", ""));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=0U2AOabo17Y$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=6fwtAX5AFRY$"));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=RArNOrKJJhE$"));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 15화", ""));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 16화", ""));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 17화", ""));
                listArr.add(new ListViewItem2("히어로즈 시즌4 - 18화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=VdrbFEj2hzM$"));
                //listArr.add(new ListViewItem2("히어로즈 시즌4 - 19화", ""));
                break;
            case "고담 시즌1" :
                listArr.add(new ListViewItem2("고담 시즌1 - 01화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/60/20140925141741718mxomm2ri9iw9w.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 02화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/71/20141002183812156g76kc5r31evox.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 03화", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/27/20141010091328578emt5e4vicf2hj.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 04화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/80/20141017095143140y3vnnuenv4tsm.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 05화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/62/201410232133040780tarky09eq532.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 06화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20141030213350859i5j1h08230cz2.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 07화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20141106091615389zujm5ni823nr2.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 08화", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/78/20141113061535343fo19splq1jhfq.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 09화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/74/20141120210901609djwwtmpb4c2ac.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 10화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/88/201411272136138599kho6wp6x4zm1.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 11화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/90/2015041614203295348kwl6w3hum5j.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/20150416142238343oacrckdrypz3c.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 13화", "http://183.110.26.41/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/04/20150416142422531fjav1nngrp7go.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 14화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/20150416142600015mx7gdi88uqyl4.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 15화", "http://183.110.26.50/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/12/201504161428071875xfrtcj3opojp.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 16화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/18/20150416143035703yqqvehsxz4sww.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 17화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/25/201504161432397180gnhdqqevi6xu.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 18화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/38/20150416143418781lmv6w23f54f0a.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 19화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/201504161450049844uvpu3jxh9g8f.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 20화", "http://183.110.25.113/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/82/201504241435512182ldadb20d4ljl.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 21화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/62/201504301725311569yqin9afrbjsn.flv"));
                listArr.add(new ListViewItem2("고담 시즌1 - 22화 완결", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/66/20150506095301859w0rljfdhyiy6l.flv"));
                break;
            case "고담 시즌2" :
                listArr.add(new ListViewItem2("고담 시즌2 - 01화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/90/20150923191646819wlqx8nvabamz1.flv"));
                listArr.add(new ListViewItem2("고담 시즌2 - 02화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/27/20151001190842847ivd72ls2vn207.flv"));
                listArr.add(new ListViewItem2("고담 시즌2 - 03화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/97/20151011130131566tvwtocmgzl5ze.flv"));
                listArr.add(new ListViewItem2("고담 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vee565DSpO55OdF5iFrdXXr"));
                listArr.add(new ListViewItem2("고담 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2e57oOvCC6vfYlbOoEm400"));
                listArr.add(new ListViewItem2("고담 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc1e7RaHlpdHcuXHcllpoTT"));
                listArr.add(new ListViewItem2("고담 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5e4ccImW11S77qcL1aIpoW"));
                listArr.add(new ListViewItem2("고담 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2b62RmwjXWXPijgR4iFjW4"));
                listArr.add(new ListViewItem2("고담 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2e78KtSscEipE3tsEtbWyn"));
                listArr.add(new ListViewItem2("고담 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vd08bZYgU8AxUKO1OUZ0eeP"));
                listArr.add(new ListViewItem2("고담 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf53cVTBxaRa90Sa7W7Y9fR"));
                listArr.add(new ListViewItem2("고담 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v33b8PjGSPabaJHYVVjVHVv"));
                listArr.add(new ListViewItem2("고담 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vbf9dsnxYxxRhIGkMxsGYyO"));
                listArr.add(new ListViewItem2("고담 시즌2 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5c17RFRhf1FNVYHF1YhhNT"));
                listArr.add(new ListViewItem2("고담 시즌2 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vf3caK6DDT6jjxZwaKawosZ"));
                break;
            case "데어데블 시즌1" :
                listArr.add(new ListViewItem2("데어데블 시즌1 - 01화", "https://d1wst0behutosd.cloudfront.net/videos/1607296.720p.mp4?Expires=1468500739&Signature=b8aiINVYxztuO-djHGDkVj6ysuM6TMyM7tyjCxyc52P~Sez4e7jSvIt8-YrEU4Pvyd5NUCFMgsyBNqdrVC1xQzMTjumd9UdlM9kQ7wW9F8FOmgqvBpvjiNTtxZgtP0IK1j79OVIY4ctBzh3lXHOlnOi84RUY~kBpdL7XdMl3OYbH31JLTChw-vdQQb5Fk4aPN4fC-LMoed2-~LktNXAcEd2spnUYbKnMM6jrSXEZsSCre5DIIsk4Edn1g9jD3PbWXUiV-uc9ghscfqmScDq-Lgxq3taIY-iFsmpsZNP0TIfln523wF7ZqOMDyEnpQCi4e4dOa-thA1NrX8y13ZTW3g__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 02화", "http://183.110.26.75/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/58/20150412174140656722jqt1us6mp3.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 03화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/2015041414430184360e3vb968yase.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 04화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/91/201504141445444843q49yikst8gv5.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 05화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/2015041414483126519vybbehet2wo.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 06화", "http://183.110.26.50/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/201504161443347960cijl7bkxw40l.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 07화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/02/20150416144600609sa92ujwf87l07.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 08화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/77/20150419130326921ypie02fkxttid.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 09화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/81/20150419130643000kwhnh4z6gz8pb.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 10화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/43/20150420080313781ko5yxq1ik9dxr.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 11화", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/20150420080640671bn6ocec7x9dya.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/73/20150422153118218zyfwlah8idul4.flv"));
                listArr.add(new ListViewItem2("데어데블 시즌1 - 13화 완결", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/87/20150422154717531ws4ztagd87mck.flv"));
                break;
            case "데어데블 시즌2" :
                listArr.add(new ListViewItem2("데어데블 시즌2 - 01화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/81/20160608185638850yckrcbrdqpear.flv?key1=41383032313334373732344131314531363030373631364146303739&key2=8E397C3604BE49008F38420C0C38F5&ft=FC&class=normal&country=KR&pcode2=52752"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 02화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/83/20160608185927141hgzgc552n3rk0.flv?key1=33384432343231394332353631313231363330374631363443304443&key2=8680A0BAD4C0F5BB8490B9F6D28EFB&ft=FC&class=normal&country=KR&pcode2=11622"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/201606101900335743beldrah5zucy.flv?key1=43433844353033393932353231313131364630374631363631353043&key2=A88ED397A497F9BCDFEA97ABDF81D4&ft=FC&class=normal&country=KR&pcode2=69408"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 04화", "http://183.110.26.84/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20160610190425595ni3ch47z8w565.flv?key1=30454641413630383432363731313231364330373631363134414545&key2=D008B36AC690084BA2826956B477A2&ft=FC&class=normal&country=KR&pcode2=74902"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/19/201606120757367088uybp6c1azii0.flv?key1=39434638383633373432363831313231364330373731364334343530&key2=AC98D490D19DEBA8DE9693D2DC92A8&ft=FC&class=normal&country=KR&pcode2=68021"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/12/20160615111314710f5tod88j7z8i4.flv?key1=44444446313235373532363731313031364430373631364632323045&key2=544358A92D1734712119A8EA5E4256&ft=FC&class=normal&country=KR&pcode2=7031"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 07화", "http://183.110.25.102/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/201606171853418416aa9d0srzn71d.flv?key1=44343631304332333432373931313331363030373631364641363632&key2=FE129C23EFCE6551FFAC216A946089&ft=FC&class=normal&country=KR&pcode2=29598"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 08화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/23/20160617185635703p6yvm9b7x3exo.flv?key1=36373533423134354332373131314631364130373031364537423237&key2=8AC0206E50BDB2F7FA64192D57B28D&ft=FC&class=normal&country=KR&pcode2=73812"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 09화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/23/20160617185635703p6yvm9b7x3exo.flv?key1=36373533423134354332373131314631364130373031364537423237&key2=8AC0206E50BDB2F7FA64192D57B28D&ft=FC&class=normal&country=KR&pcode2=73812"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 10화", "http://183.110.25.103/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/78/20160621064601797wtxqoikzz5fht.flv?key1=43414633384232314432384231313931363830373031364436423939&key2=AC1A09F17D926951A543F9CA0563DB&ft=FC&class=normal&country=KR&pcode2=82639"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 11화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/59/20160625101606120hw8cs84rr9fk0.flv?key1=33354330353434323332384531313931364230374631364236453545&key2=0065FAC7F942665173BFCF81866275&ft=FC&class=normal&country=KR&pcode2=33938"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 12화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/76/20160626090150684uh0u7y3yyo26z.flv?key1=38304239413330324532393131314131364330374431364539413830&key2=C033AA37DA863500B29E477ED543B3&ft=FC&class=normal&country=KR&pcode2=27609"));
                listArr.add(new ListViewItem2("데어데블 시즌2 - 13화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20160628065232682wh1k3woe92lut.flv?key1=42383737314231394632393831313631364430373331363934323842&key2=CCE4BE2DC2F9E9DAB9892A66CB9FB6&ft=FC&class=normal&country=KR&pcode2=14940"));
                break;
            case "바이닐 시즌1" :
                listArr.add(new ListViewItem2("바이닐 시즌1 - 01화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/86/201602272212221557pkl7yj1woxdq.flv"));
                listArr.add(new ListViewItem2("바이닐 시즌1 - 02화", "http://183.110.26.45/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/20160302065743345udnacm3qfr7zu.flv"));
                listArr.add(new ListViewItem2("바이닐 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/90/20160312085737041m5nykoszj8742.flv"));
                listArr.add(new ListViewItem2("바이닐 시즌1 - 04화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/86/201607040824393169cshendcplywm.flv"));
                listArr.add(new ListViewItem2("바이닐 시즌1 - 05화", "http://183.110.25.103/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/75/20160707190100560yxkfbuv6cglax.flv"));
                break;
            case "샨나라 연대기 시즌1" :
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 01 ~ 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/20160113193509671adoi27lugp4d1.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 03화", "http://183.110.25.114/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/56/20160117101004963mc7upfozscnh7.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 04화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/51/20160131161956111trshdhoptif7z.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 05화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/89/201602020637449243it4tcfmpbygj.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 06화", "http://183.110.26.45/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/77/20160205215422725gg9dv23n04hpv.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/91/20160213094400634ufc8cshytn11n.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 08화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/66/20160221094947985ea80zl05l1tqx.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 09화", "http://183.110.25.105/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/59/201602261832430752hl6ea9qmfmot.flv"));
                listArr.add(new ListViewItem2("샨나라 연대기 시즌1 - 10화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/09/201603040620121184hs50mntq6gkc.flv"));
                break;
            case "빅뱅 이론 시즌1" :
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 01화", "http://drama.cd/files/videos/2015/04/26/1430017560badf6-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 02화", "http://drama.cd/files/videos/2015/04/26/14300176223b95d-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 03화", "http://drama.cd/files/videos/2015/04/26/14300176726934c-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 04화", "http://drama.cd/files/videos/2015/04/26/143001771141b67-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 05화", "http://drama.cd/files/videos/2015/04/26/1430017745fc6ed-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 06화", "http://drama.cd/files/videos/2015/04/26/14300177918110c-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 07화", "http://drama.cd/files/videos/2015/04/26/1430017837a640b-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 08화", "http://drama.cd/files/videos/2015/04/26/1430017888635a1-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 09화", "http://drama.cd/files/videos/2015/04/26/14300179345f558-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 10화", "http://drama.cd/files/videos/2015/04/26/1430017973de6d0-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 11화", "http://drama.cd/files/videos/2015/04/26/1430018010b012a-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 12화", "http://drama.cd/files/videos/2015/04/26/14300180402c5f2-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 13화", "http://drama.cd/files/videos/2015/04/26/143001807127495-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 14화", "http://drama.cd/files/videos/2015/04/26/143001809919ca7-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 15화", "http://drama.cd/files/videos/2015/04/26/1430018167cf71b-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 16화", "http://drama.cd/files/videos/2015/04/26/1430018212f4c2b-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌1 - 17화", "http://drama.cd/files/videos/2015/04/26/143001825177d95-sd.mp4"));

                break;
            case "빅뱅 이론 시즌2" :
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 01화", "http://drama.cd/files/videos/2015/04/26/1430018287894da-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 02화", "http://drama.cd/files/videos/2015/04/26/143001834012c7a-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 03화", "http://drama.cd/files/videos/2015/04/26/14300184044ffce-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 04화", "http://drama.cd/files/videos/2015/04/26/1430018493f63c3-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 05화", "http://drama.cd/files/videos/2015/04/26/14300185466c117-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 06화", "http://drama.cd/files/videos/2015/04/26/143001859092bc9-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 07화", "http://drama.cd/files/videos/2015/04/26/14300186264ae5d-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 08화", "http://drama.cd/files/videos/2015/04/26/14300186615c5e3-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 09화", "http://drama.cd/files/videos/2015/04/26/14300187348a5ad-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 10화", "http://drama.cd/files/videos/2015/04/26/14300187732288a-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 11화", "http://drama.cd/files/videos/2015/04/26/1430018809491a3-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 12화", "http://drama.cd/files/videos/2015/04/26/1430018849372f5-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 13화", "http://drama.cd/files/videos/2015/04/26/14300189342180e-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 14화", "http://drama.cd/files/videos/2015/04/26/1430018986d40b6-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 15화", "http://drama.cd/files/videos/2015/04/26/1430019042e2862-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 16화", "http://drama.cd/files/videos/2015/04/26/143001908748871-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 17화", "http://drama.cd/files/videos/2015/04/26/143001912330f18-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 18화", "http://drama.cd/files/videos/2015/04/26/143001918949239-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 19화", "http://drama.cd/files/videos/2015/04/26/1430019248125de-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 20화", "http://drama.cd/files/videos/2015/04/26/1430019292b19cb-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 21화", "http://drama.cd/files/videos/2015/04/26/1430019331a0202-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 22화", "http://drama.cd/files/videos/2015/04/26/1430019369f63ac-sd.mp4"));
                listArr.add(new ListViewItem2("빅뱅 이론 시즌2 - 23화 완결", "http://drama.cd/files/videos/2015/04/26/1430019416e19bb-sd.mp4"));
                break;
            case "리미트리스 시즌1" :
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 01화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/35/20150924185849081c46ha183ze43i.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/03/20151003162917466lium4v0k8qhhx.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/18/20151010071743401kj3iyjjayqt73.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/08/201510170713170487uff8qggfrtfu.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/69/20151024173835055lzz54uibskwq2.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/04/201510291929221873ug38p0p7gzx4.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/53/20151114065637239yctaxsreoe8g1.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/57/20151114065849822k9nf0ednzrxjr.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/99/20151121153952084i97p6lg0t9pfz.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/19/20151127184232179r7lqku54zfobo.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/55/201512221904398541z4b0y6hssm24.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/201601121848452594q1te1aiwkr9y.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20160121184711502s3kp44zgdyte4.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 14화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/20160222175039634z9ubkm68a8b1d.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 15화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/04/201602231840265004cp6drb8atiqz.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 16화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/201602290618340805kqqqpd3in8ea.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 17화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/29/20160405223720301j1fe7voqywe1m.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 18화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/12/20160407065408865yiyabqfx94423.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 18화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/19/20160408063652796b8pdl9je2cf04.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 20화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/52/20160409181517382cfjbunrmil5vk.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 21화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/03/20160424123912641duiqadfw064tq.flv"));
                listArr.add(new ListViewItem2("리미트리스 시즌1 - 22화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/00/201605291136039769qx6hr98fmw7y.flv?key1=39413739444333323633323731324631373230373931363442414244&key2=668270FA755088C364448EB0708410&ft=FC&class=normal&country=KR&pcode2=57723&px-bps=3050701&px-bufahead=10&cms=1"));
                break;
            case "루시퍼 시즌1" :
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/09/20160131085357918wkk9mlg3fxril.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/38/20160211092155115jbax5hikmsgo6.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/42/201602110924242954fzkpvtuty0mc.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/10/201602221754175213ug02jcpmeoor.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/18/20160226181836430uo1yicn16e70j.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v5f48d6RqdqRc5uymducqmu"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/99/20160317094206044u8bg1q0u0c1uf.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/50/20160321071427047ff6q228odhqri.flv"));
                listArr.add(new ListViewItem2("루시퍼 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/58/20160403083124773cig6ybe4p2z4c.flv"));
                break;
            case "주(Zoo) 시즌1" :
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/67/20150711101923165k7sfhj75qunhg.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/14/20150711104541097rgxg1ltnhhi3z.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/22/201507191746007820c0dvzvx5am64.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/03/20150731171013702lrir9xe822gsu.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/77/20150803114306051awcxg9fvy43dn.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/83/20150811135328356yfy8p6y22nl14.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/49/20150816151518362vq5mu1f7wfg9s.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/59/201508240651061250y1hg7278tj94.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/2015083106262377699g1ybzmlu752.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/20150906083519501tv399umlr4byh.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/75/20150914065003521eis36vn6m6mfj.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/87/20150914065234648w7mfa34d2ej3i.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌1 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/49/20150920043557201k88m53ied5i09.flv"));
                break;
            case "주(Zoo) 시즌2" :
                listArr.add(new ListViewItem2("주(Zoo) 시즌2 - 01 ~ 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/20160706190653029ciub0g5lbes62.flv"));
                listArr.add(new ListViewItem2("주(Zoo) 시즌2 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/58/20160710102739855e1fwxu166x3n2.flv"));
                break;
            case "플래시 시즌1" :
                listArr.add(new ListViewItem2("플래시 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/sd/_user/l/p/lpg451/73/201410201911078123xcbmf7etkano.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/2014101709383664052f4s4hnepudq.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/45/20141024214622328dpnuzvn7gybur.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/10/20141103073523312n52rfgs1zhhsh.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 05화", "https://d1wst0behutosd.cloudfront.net/videos/1763800.mp4?Expires=1468753773&Signature=SuqcxRcfgh~44DvnE7rfWI6~uvzm1skUlhQWVrI3frj2k~4QWt0~tEZrfiruC6AYw3R6qPayaqG1Q-ktZw2fSQqDGJy5Wo8mIBKMfxoMNmAOfQFbCOVkZclW2Ygmm4oVhokQt5jk0qK9SgplPm0W02hrosLlyuMEqD1y4zfYKznqctgGYuSEwRDfGas-6zZVkN8RDbK3KS2ru4KqrzOSk1AAhKHWtAXiLvA7vDda1XOK6TdgM9~hFyd6QVN3afoUhmE0rnGRMCOEdMrLthbjW~nBoj-BC2scLeb9rMdLqZrWPNxqlfnO6t9fgh7nGGyCEr2AavL6jGHNGbalvf76xQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/06/201411212127503591cmdll2lav2wx.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/33/20141129215753687bdigmp22uv5ty.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/39/20141206091957468olle6oknsnyhd.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/25/20141211221048156jw7qwmrsgsr2f.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 10화", "https://d1wst0behutosd.cloudfront.net/videos/1764160.mp4?Expires=1468753872&Signature=AqzWrx2Nl1kUmpzuUm5FSwczkatpiws96fWeJ1mbjnQMoZgaoRTrEj1Pj1HPgoFwnrf7qsofmF3LijEwq6nceKTBEiq9WZtm-FgEThY7Y627XTqtBIo2KOqNwLhaCKdVbInEKm1lZtDMEuDlI7b-0NsUcWfW1IYagAySletqxpnWIT7K5X-oZ1MkLgNLvxA-hYFa4E8gAi73MbQy3-5owQzZiNjGk3pt5CYNo93odizymHPB-WYVnmdZrT~QVm~~FRMAf9Uqomzw~78uUKXj5yBhVHaWIpbiCOB0WSk4dcgEQvAF5kNSayMAx-9Zwqz3vdSv~4CyiK~QQ8GSBzkufg__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 11화", "https://d1wst0behutosd.cloudfront.net/videos/1764169.mp4?Expires=1468754048&Signature=Mkr5Cn-DNTWiyY5JX3QiAyf5qY8vMLThMBCYPQD2tDrFY73UNKFK4NMr1I5KIdlCNFP4Zc2OTkrCgIdhq4NGYpo3O0qKOqxxiqlSo0M9KQFEBcm2zEyhcY3rNarQe43gDrrr~u937PbXRJLH5FCLZYwiU-KcZ0kwGSqirkHGf9qrZIqiYakyaslnstqtKCvvndrOO~wlaODeNZ7EmFHgtsu-y1nxV1HbKNjzT658oqBbZcn-xpRLeXfSUP3ykHjc23ZG7Nrasom64nZ~xbtuZWulmlnojvfLlAhULwpt6DU3O9AxY0KfufRCOHBhm~f6zNo5sgQ4vgo3uAiU4RCV4A__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 12화", "https://d1wst0behutosd.cloudfront.net/videos/1764190.mp4?Expires=1468754064&Signature=THB8ddC0NVWEQAjkeIFFAIL7N-M6JTVdk-d2-Avft2FfNGh2Ep55caJloERxJ43t4K8SR87QTGpsyjLRWxznLuNm7om-wMROxlKZbECNz3c13N8TZ8poksTecPVoH~~uU4DDZNqWceFHzxukJEzwDUqefu2KM39v7~4dZyZjClKwtb901eWxBrnLNjbjg01NaWrKZksLmeatHuNf2KEVBs00e1Yd5Tj6616FZ4O3cswAsmZxmzdi0qEId7HP3YiyEaPlZy36LMUyeBLK~RG5uUberyl9TdiMRTafhiaRGRTQQRFy6KUUiwpeICrpx~3VPguSX7Eufmgb4eg4ZAik2A__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 13화", "https://d1wst0behutosd.cloudfront.net/videos/1764199.mp4?Expires=1468754080&Signature=JpBjd78V~dEFqrR9Pw7FPvauIiNRtcLb0QHUZv91gHHiV8verWxpk0z9-STTzjGZ-O9R~Be-AUHyRStV8b~IH2bJvT-x4Zc8W96PCutKGoF73yOqE5WjPXIk393z5y6FTvhJqPV5~5KACCoTdl6nPKaYtxL8Xtl57yHTqECvCl7j4~UWWj5EUomP664zDYcJTXSpLkuieJKu0he8iRuTc8ssXt2sLo9Q74KSoBCvfta1tf7JD12h5ZNSP2HTJKiDp-DeBc-6wa59dVOoVRsddrNeSDR24fYJ7GyInK5zrdzTHIEdTUVahxNCfnODBdWXvXehnTrgXPnq6-s~vCpOYQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 14화", "https://d1wst0behutosd.cloudfront.net/videos/1764700.mp4?Expires=1468754099&Signature=NBRmQXDFPZjRjz2zaZ0P9qo3Wo9LT63hOtocW8pE-QYfylBOH3IyTLAcYkev3K9NYXFfutWI61qcC4jQHvF5VulosyU0sF1HqZh0tj9wkHId0yTt1Z22l2TRD7uXOC1Movr~0PMfvI4wyLw2AghCuk6XXBVbUNASReCfXe9sT2k-Zn7d0V-QtPHdl4IfVr8gESafCICNwLr5MYwvTLyD7p60AtvawsgADlReKBL4IPShIlnqt~OTwSd7Oc1zyN2WMDAHNpjrBqa-CFXK6wWM5pDGU5SuhdUANS2EMXD5V7i7cnWwGi9pCevJULdotdzVJGnDNez7a3aRMop2Yfmq8A__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 15화", "https://d1wst0behutosd.cloudfront.net/videos/1764703.mp4?Expires=1468754115&Signature=PMFFAK8Cbk-yckUzw2wAbX7HKUCd~P~ZGFuNsrACB84fBK~GpLzbLgs78o1xABTdN3wsIFJRXaFRIMNJ9jazUNibxL4ywm0SnBWWOMCBbAkZcBWOarYBzMHqvH3EipMQYNpH7GOjCUuWIOTDE8ecaF0tOUGX43ZMjPCgHB-6XuN2MNxHp~TzA0k7IGnJM5YYK~O18N2BNTPbnLldZVjieC3clVV1LsgvgRuZ~ayo~hhHF~b6Q4pxoBvTE2WJ2BL1ogeHo4nzMeBAmbQlimuTyuX0CfOYtI1fL23F75NW6769Wn3zdWVpFdW44QYNDYUgMtSl6vPVQ-aUKxXZptUppQ__&Key-Pair-Id=APKAJJ6WELAPEP47UKWQ"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 16화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/72/20150326163338089o7l3hwwrlcgso.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 17화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/52/20150402144515296dn6g0xguxk8yb.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 18화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/08/201504181334158904ihvdqlkch9s2.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 19화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/07/20150424144705640onm60p14ffp45.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 20화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/10/20150430173229718iuk95u6qp0zbs.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 21화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/42/2015050717314475066xpz5uvoo1qs.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 22화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/37/20150514083146656ksrpfwh499tuz.flv"));
                listArr.add(new ListViewItem2("플래시 시즌1 - 23화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/78/20150521051748859t8la7ulpg9p4u.flv"));
                break;
            case "플래시 시즌2" :
                listArr.add(new ListViewItem2("플래시 시즌2 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/10/201510090728191405i3zzkrkivvnz.flv"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc971gVtkbQ5cOp5QRh5Vhk"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vfae5t9o9otlocY4A4ySAAw"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v96fbchriLWUriA5u5SUMh5"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vbc324CZc4iZCdv8cauzLSC"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v565eMYGsGZHYf2HnOYFf1Z"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v8779XJTFat08JkX6FJX1vv"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vff1dYqhpY8pY7I89Y8hIiS"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v4097AbyAyJAoo3FQtyBZQZ"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vd6099ehh7ewwrirvawb755"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vc2d0nWgmJiRsbBbGJnxmwm"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v6850bzSmb1XUUAU61mCbEg"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v9255Z1ZDPjZ5jKjW1Ajryu"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 14화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v2b3bvIq5vrvbhy0hqp50IX"));
                listArr.add(new ListViewItem2("플래시 시즌2 - 15화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v7f9244xxUpyRvWPBpFzpww"));
                break;
            case "마이너리티 리포트 시즌1" :
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/02/20150818061820120zdfojqbnvt2tb.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/20/20151010072029231s11kpmuq0ui6o.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/201510150620272042z8ofeoi1b7qn.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20151016065231289tkkhg9dcwwwwl.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/45/201510260945565368nrs5cb1egokq.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/70/201511020612396498sez037x5yelm.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/27/20151123062427315lrqxim2stw5u1.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/82/20151123180629676wtgds7h14o5wh.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/85/20151203071206291h3wborito8veq.flv"));
                listArr.add(new ListViewItem2("마이너리티 리포트 시즌1 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/02/20151206073917906fuyi1m52sitmy.flv"));
                break;
            case "익스팬스" :
                listArr.add(new ListViewItem2("익스팬스 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/48/20151228065318468x73mbnzax294d.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/89/20151230214317188875rdh8upc0je.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/20160101153805415ptu9mdlstw8qq.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/19/20160101154016842k2fjm38628hie.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/60/201601080659281995ol8ot29vdn24.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/53/20160115191238541mu5j6x5a8ujkv.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/42/201601250651387164sycohs2u93xx.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/51/20160206231630517d0zxiklg6u5uv.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/03/20160206222001888kld8ei7avedqn.flv"));
                listArr.add(new ListViewItem2("익스팬스 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/98/20160206160201801zbv6twl5r1den.flv"));
                break;
            case "DC 레전드 오브 투모로우" :
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/73/20160124110522038ltssafk5b5se7.flv"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/20160202063527726kja2aio8qnjho.flv"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/53/20160207110716115ior9c0ubmqtnq.flv"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/30/20160211091907324zxx2j9tltp3xn.flv"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vd56fBzbX9BlO5MOOles5zh"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=v44e1qmI0WEKKMs8bANIbN8"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=va530yZMx3y6yUZTnMZexQx"));
                listArr.add(new ListViewItem2("DC 레전드 오브 투모로우 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=vcd5cNEy0scV9t9EyAKsKcj"));
                break;
            case "닥터후 시즌1" :
                listArr.add(new ListViewItem2("닥터후 시즌1 - 01화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20101102062052680prfshv0xj7a1a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 02화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/70/201011020627104155sth4krtt8k74.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 03화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201011020627330718rlbjzxwyxv30.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 04화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/96/2010110206275914977os5gjx0ftm3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 05화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/05/20101102062847946jkuoj3evh6pzl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 06화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/02/20101102062052712s4gmaxscsoxl7.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 07화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/03/20101102062153712pg5rm1je5i355.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/09/20101102062639774j628x3xwojfyc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 09화", "http://183.110.26.41/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/13/20101102062609399ismf2gsa9k4zp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 10화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/14/20101102062820165fsghcoekv8gc9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 11화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/20/20101102062819212zvcxd9gc59c17.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 12화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/22/201011020628214627ucusbw6jggmd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 13화 완결", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101102062821165qvct2l9cbi0om.flv"));
                break;
            case "닥터후 시즌2" :
                listArr.add(new ListViewItem2("닥터후 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=2Sq8JbsnzQI$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=9XoCsXkR508$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 03화", "http://183.110.26.90/redirect/cmvs.mgoon.com/storage2/m2_video/2010/08/31/4091263.mp4?px-bps=1385829&px-bufahead=10"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ZDLbDSIYoVg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wcfbGqBXWUA$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=0XeFNvG2s6c$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=51mTbbPiSK8$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=u59WN-VM9Sg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Zu1XSDK0qFc$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=QHbBhXKVd9A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=upbzVrA2dJk$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=74EWwOaaO0A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=zwIpR7dAd7g$"));
                break;
            case "닥터후 시즌3" :
                listArr.add(new ListViewItem2("닥터후 시즌3 - 01화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/48/201011010326395240mjm5qroezf3j.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 02화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/60/20101101032616509622osc0g1bs6a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 03화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/71/20101101032617368s591udn5n1zi0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 04화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/90/20101101032627290lavqi4nmfvqlc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 05화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/30/20101028094042059lvglsref8gcbd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 06화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/42/20101029054303371ry1vormdpmtpi.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/54/20101029054241871rww2isbyx1yqr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/20100926064207251vmzxfx8dkxv5w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 09화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/61/201009260649134549q1bd7setgtgy.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 10화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/201010290554380436067z5v4ds30w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 11화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/81/20101029055458871idpvouyrzj4ro.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 12화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/00/20101029055441340gjlqgmtjq4yli.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 13화 완결", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101029055442121ixr0pg9u4dcpt.flv"));
                break;
            case "닥터후 시즌4" :
                listArr.add(new ListViewItem2("닥터후 시즌4 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/2010080218202800960u4kge1j1zjl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/89/20101029024502559o7mfcy9kdnqu0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 03화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/12/20101029023649902we40xzi2cb6sk.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 04화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/69/201010280810091990v1cpawxy53oq.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 05화", "http://183.110.26.75/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/06/20101028075358496vlv0dh6ui34eg.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 06화", "http://183.110.26.82/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/51/201310032059365185zz0ia2dgysh5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 07화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/42/20101028052027527r7g76gn76d3d4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 08화", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/60/201010280514077319b6mfwwmgpa3h.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 09화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/85/201010280514107461yv1zqjopvufz.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 10화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/26/20101027093734418axsf5g315qeuf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 11화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/11/20101027092320762p5yy8m30luwmr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 12화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/26/20101027080600231gdmi9318ve6aw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 13화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/01/20101027062633387a68rbg7mbtfr3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌4 - 14화 완결", "http://183.110.25.95/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/06/20101027062141981pfd6yybg6l81b.flv"));
                break;
            case "닥터후 시즌5" :
                listArr.add(new ListViewItem2("닥터후 시즌5 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/57/2010091205592524001cnnxgm6kv4n.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/04/20100913054720537g19stf5xzztah.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/29/201007280831388542c5xx77lnlyg5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/37/20100904073508064rvt4wdlgyp9n4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/17/20100907093504587ap4h8msvmbxkr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/25/20100616054111525orskvuobvv1cf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/59/20100522114114000qkr4wpmjaqa01.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/15/20100529205355468vbyfn0ai2m5wm.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/20101219100328507imdbqcm3h43dp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/89/20101219104835179xnt13ep7qi57b.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/37/20101219105704648vvvvst8fv9aub.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/53/20101219110455663c25bme90wjke2.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌5 - 13화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/11/201012191128238207wq6wp80b6y3w.flv"));
                break;
            case "닥터후 시즌6" :
                listArr.add(new ListViewItem2("닥터후 시즌6 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/79/20121101010506925382ctzv77n1iy.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201211010115199565heu5fpk7tid2.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/81/20121101012107534zxfjnka07tmi9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 04화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/16/20121101013012909zy7cr6o6k0wog.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/17/20121101013030175ve51zpgaspuxg.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 06화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/18/20121101013032925tdc07elhbkdnv.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/51/20121101083352768y9mtryl4xc4mu.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 08화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20121101083353518480hiriqhbfy8.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/53/20121101083354065l3nq6vglh6ja1.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/54/20121101083354471o3o67kz5ufevf.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/62/201211010922203467to49t3v3xrib.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/63/2012110109222083153dppg8berbgc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 13화 완결", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/64/20121101092221331joy3ubesvs3a0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌6 - 크리스마스 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/60/20130224213418040lhsbx7dzqkcng.flv"));
                break;
            case "닥터후 시즌7" :
                listArr.add(new ListViewItem2("닥터후 시즌7 - 01화", "http://drama.cd/files/videos/2015/04/24/1429863602b8741-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 02화", "http://drama.cd/files/videos/2015/04/24/1429863677e6f59-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 03화", "http://drama.cd/files/videos/2015/04/24/14298637506b716-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 04화", "http://drama.cd/files/videos/2015/04/24/1429863860bfcc8-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 05화", "http://drama.cd/files/videos/2015/04/24/1429863927d0ea0-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 06화", "http://drama.cd/files/videos/2015/04/24/14298641681bc6e-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 07화", "http://drama.cd/files/videos/2015/04/24/142986422541bb8-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 08화", "http://drama.cd/files/videos/2015/04/24/14298643394cc9c-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 09화", "http://drama.cd/files/videos/2015/04/24/14298644274cc17-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 10화", "http://drama.cd/files/videos/2015/04/24/1429864538f2f1d-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 11화", "http://drama.cd/files/videos/2015/04/24/142986462571a96-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 12화", "http://drama.cd/files/videos/2015/04/24/1429864692e0c3b-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 13화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/33/20130520153521155e7tze4mdpargw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 50주년 기념 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/74/20131125195901638pkdrb9niv9bb3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 더 파이브 이쉬 닥터 리뷰", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/43/20131205170523804h20545hbhxrlo.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌7 - 크리스마스 스페셜", "http://drama.cd/files/videos/2015/04/24/1429864896e6b6b-sd.mp4"));
                break;
            case "닥터후 시즌8" :
                listArr.add(new ListViewItem2("닥터후 시즌8 - 01화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/28/20140826055210171jidc5wguygrxa.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 02화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/41/20140901140949093mxfm1ech6noft.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 03화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/96/201409081630399531o4fazsw5hcwl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 04화", "http://drama.cd/files/videos/2015/04/25/142992907281f1f-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/77/201409212048548595vx0ulz0cx3dr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 06화", "http://drama.cd/files/videos/2015/04/25/1429929345001ec-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 07화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/27/20150114093727466wkqrbk0dwyaz0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 08화", "http://drama.cd/files/videos/2015/04/25/1429929643c37e1-sd.mp4"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 09화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/02/20141020070342640l8zm6d8smkys4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 10화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/27/20141027172813156zt9x6hy8ja7l4.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/86/201411021844548902npwmoeab6u7m.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 12화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/06/20141109163646298gt5x308ctltnw.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 크리스마스 스페셜", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/c/o/codeno001/49/20141226181536914wm6cyrujii2ij.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌8 - 50주년 다큐드라마", "ttp://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/79/20131125175456531feasi0d5cblfb.flv"));
                break;
            case "닥터후 시즌9" :
                listArr.add(new ListViewItem2("닥터후 시즌9 - 01화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/79/20150922145640369zosw0h6zg86qx.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 02화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/u/dudem/82/20150927155628374lc6wvjpdwtqzr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 03화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/62/20151004131654148vhplg2afdseff.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 04화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/201510120625369193wjfezu5ukfwj.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/90/20151018145700000t0j5apbwirwdt.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 06화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/27/201510260937547490dddfafn1kbf5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/27/20151108165755779vict6n3m3rvwl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 08화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151110212455179wbojkkbfv66ss.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 09화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20151123061709623ebv1hrbz6juct.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 10화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/20151123061948349txxd71u3fgq43.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151130065212189omdtriwotfjfx.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 12화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/20151207060229328jczji0n06zzng.flv"));
                listArr.add(new ListViewItem2("닥터후 크리스마스 스페셜", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20151227122640516weyx8p6hrojt0.flv"));
                break;
        }



        return listArr;
    }


}
